Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 1, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 7, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=27615):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     27488  -->    27448        40
		False      127  -->      118         9
	rates:
		              True    False
		----------  ------  -------
		sample       0.995    0.005
		population   0.005    0.995
	match_rate (micro=0.075, macro=0.5):
		  True    False
		------  -------
		 0.929    0.071
	filter_rate (micro=0.925, macro=0.5):
		  True    False
		------  -------
		 0.071    0.929
	recall (micro=0.075, macro=0.535):
		  True    False
		------  -------
		 0.999    0.071
	!recall (micro=0.994, macro=0.535):
		  True    False
		------  -------
		 0.071    0.999
	precision (micro=0.995, macro=0.502):
		  True    False
		------  -------
		 0.005        1
	!precision (micro=0.01, macro=0.502):
		  True    False
		------  -------
		     1    0.005
	f1 (micro=0.132, macro=0.071):
		  True    False
		------  -------
		  0.01    0.132
	!f1 (micro=0.01, macro=0.071):
		  True    False
		------  -------
		 0.132     0.01
	accuracy (micro=0.075, macro=0.075):
		  True    False
		------  -------
		 0.075    0.075
	fpr (micro=0.006, macro=0.465):
		  True    False
		------  -------
		 0.929    0.001
	roc_auc (micro=0.835, macro=0.836):
		  True    False
		------  -------
		 0.837    0.835
	pr_auc (micro=0.985, macro=0.559):
		  True    False
		------  -------
		 0.129    0.989
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

