Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 1, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 3, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=27615):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       298  -->       42       256
		False    27317  -->      180     27137
	rates:
		              True    False
		----------  ------  -------
		sample       0.011    0.989
		population   0.011    0.989
	match_rate (micro=0.981, macro=0.5):
		  True    False
		------  -------
		 0.008    0.992
	filter_rate (micro=0.019, macro=0.5):
		  True    False
		------  -------
		 0.992    0.008
	recall (micro=0.984, macro=0.567):
		  True    False
		------  -------
		 0.141    0.993
	!recall (micro=0.15, macro=0.567):
		  True    False
		------  -------
		 0.993    0.141
	precision (micro=0.982, macro=0.59):
		  True    False
		------  -------
		 0.189    0.991
	!precision (micro=0.197, macro=0.59):
		  True    False
		------  -------
		 0.991    0.189
	f1 (micro=0.983, macro=0.577):
		  True    False
		------  -------
		 0.161    0.992
	!f1 (micro=0.17, macro=0.577):
		  True    False
		------  -------
		 0.992    0.161
	accuracy (micro=0.984, macro=0.984):
		  True    False
		------  -------
		 0.984    0.984
	fpr (micro=0.85, macro=0.433):
		  True    False
		------  -------
		 0.007    0.859
	roc_auc (micro=0.851, macro=0.851):
		  True    False
		------  -------
		 0.851    0.851
	pr_auc (micro=0.988, macro=0.539):
		  True    False
		------  -------
		  0.08    0.998
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

