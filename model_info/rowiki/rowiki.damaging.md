Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19700):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       928  -->      754       174
		False    18772  -->     1188     17584
	rates:
		              True    False
		----------  ------  -------
		sample       0.047    0.953
		population   0.05     0.95
	match_rate (micro=0.86, macro=0.5):
		  True    False
		------  -------
		   0.1      0.9
	filter_rate (micro=0.14, macro=0.5):
		  True    False
		------  -------
		   0.9      0.1
	recall (micro=0.931, macro=0.875):
		  True    False
		------  -------
		 0.812    0.937
	!recall (micro=0.819, macro=0.875):
		  True    False
		------  -------
		 0.937    0.812
	precision (micro=0.96, macro=0.695):
		  True    False
		------  -------
		 0.401     0.99
	!precision (micro=0.43, macro=0.695):
		  True    False
		------  -------
		  0.99    0.401
	f1 (micro=0.941, macro=0.75):
		  True    False
		------  -------
		 0.537    0.962
	!f1 (micro=0.558, macro=0.75):
		  True    False
		------  -------
		 0.962    0.537
	accuracy (micro=0.931, macro=0.931):
		  True    False
		------  -------
		 0.931    0.931
	fpr (micro=0.181, macro=0.125):
		  True    False
		------  -------
		 0.063    0.188
	roc_auc (micro=0.957, macro=0.956):
		  True    False
		------  -------
		 0.956    0.957
	pr_auc (micro=0.975, macro=0.775):
		  True    False
		------  -------
		 0.553    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

