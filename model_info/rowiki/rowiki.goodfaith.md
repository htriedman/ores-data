Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19700):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     19152  -->    18241       911
		False      548  -->      118       430
	rates:
		              True    False
		----------  ------  -------
		sample       0.972    0.028
		population   0.97     0.03
	match_rate (micro=0.904, macro=0.5):
		  True    False
		------  -------
		  0.93     0.07
	filter_rate (micro=0.096, macro=0.5):
		  True    False
		------  -------
		  0.07     0.93
	recall (micro=0.947, macro=0.869):
		  True    False
		------  -------
		 0.952    0.785
	!recall (micro=0.79, macro=0.869):
		  True    False
		------  -------
		 0.785    0.952
	precision (micro=0.973, macro=0.666):
		  True    False
		------  -------
		 0.993    0.338
	!precision (micro=0.358, macro=0.666):
		  True    False
		------  -------
		 0.338    0.993
	f1 (micro=0.957, macro=0.723):
		  True    False
		------  -------
		 0.972    0.473
	!f1 (micro=0.488, macro=0.723):
		  True    False
		------  -------
		 0.473    0.972
	accuracy (micro=0.947, macro=0.947):
		  True    False
		------  -------
		 0.947    0.947
	fpr (micro=0.21, macro=0.131):
		  True    False
		------  -------
		 0.215    0.048
	roc_auc (micro=0.959, macro=0.96):
		  True    False
		------  -------
		 0.959    0.961
	pr_auc (micro=0.983, macro=0.745):
		  True    False
		------  -------
		 0.999    0.491
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

