Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=38733):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     37555  -->    34932      2623
		False     1178  -->      355       823
	rates:
		              True    False
		----------  ------  -------
		sample       0.97     0.03
		population   0.954    0.046
	match_rate (micro=0.864, macro=0.5):
		  True    False
		------  -------
		 0.901    0.099
	filter_rate (micro=0.136, macro=0.5):
		  True    False
		------  -------
		 0.099    0.901
	recall (micro=0.919, macro=0.814):
		  True    False
		------  -------
		  0.93    0.699
	!recall (micro=0.709, macro=0.814):
		  True    False
		------  -------
		 0.699     0.93
	precision (micro=0.954, macro=0.655):
		  True    False
		------  -------
		 0.985    0.326
	!precision (micro=0.356, macro=0.655):
		  True    False
		------  -------
		 0.326    0.985
	f1 (micro=0.933, macro=0.701):
		  True    False
		------  -------
		 0.957    0.444
	!f1 (micro=0.468, macro=0.701):
		  True    False
		------  -------
		 0.444    0.957
	accuracy (micro=0.919, macro=0.919):
		  True    False
		------  -------
		 0.919    0.919
	fpr (micro=0.291, macro=0.186):
		  True    False
		------  -------
		 0.301     0.07
	roc_auc (micro=0.935, macro=0.935):
		  True    False
		------  -------
		 0.935    0.935
	pr_auc (micro=0.969, macro=0.7):
		  True    False
		------  -------
		 0.996    0.404
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

