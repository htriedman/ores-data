Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=38733):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1771  -->     1406       365
		False    36962  -->     3554     33408
	rates:
		              True    False
		----------  ------  -------
		sample       0.046    0.954
		population   0.05     0.95
	match_rate (micro=0.833, macro=0.5):
		  True    False
		------  -------
		 0.131    0.869
	filter_rate (micro=0.167, macro=0.5):
		  True    False
		------  -------
		 0.869    0.131
	recall (micro=0.898, macro=0.849):
		  True    False
		------  -------
		 0.794    0.904
	!recall (micro=0.799, macro=0.849):
		  True    False
		------  -------
		 0.904    0.794
	precision (micro=0.954, macro=0.644):
		  True    False
		------  -------
		 0.301    0.988
	!precision (micro=0.335, macro=0.644):
		  True    False
		------  -------
		 0.988    0.301
	f1 (micro=0.919, macro=0.69):
		  True    False
		------  -------
		 0.436    0.944
	!f1 (micro=0.461, macro=0.69):
		  True    False
		------  -------
		 0.944    0.436
	accuracy (micro=0.898, macro=0.898):
		  True    False
		------  -------
		 0.898    0.898
	fpr (micro=0.201, macro=0.151):
		  True    False
		------  -------
		 0.096    0.206
	roc_auc (micro=0.932, macro=0.932):
		  True    False
		------  -------
		 0.933    0.932
	pr_auc (micro=0.967, macro=0.697):
		  True    False
		------  -------
		 0.399    0.996
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

