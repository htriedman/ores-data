Model Information:
	 - type: RandomForest
	 - version: 0.9.1
	 - params: {'scale': True, 'center': True, 'labels': ['Stub', 'Start', 'C', 'B', 'GA', 'FA'], 'multilabel': False, 'population_rates': None, 'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': None, 'criterion': 'gini', 'max_depth': None, 'max_features': 'log2', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 3, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 320, 'n_jobs': None, 'oob_score': False, 'random_state': None, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=454):
		label      n         ~Stub    ~Start    ~C    ~B    ~GA    ~FA
		-------  ---  ---  -------  --------  ----  ----  -----  -----
		'Stub'    80  -->       46        21    13     0      0      0
		'Start'   75  -->       25        25    22     3      0      0
		'C'       86  -->        0         6    67    10      2      1
		'B'       73  -->        0         1     8    40     22      2
		'GA'      75  -->        0         0     2    17     38     18
		'FA'      65  -->        0         0     0     1     17     47
	rates:
		          'Stub'    'Start'    'C'    'B'    'GA'    'FA'
		------  --------  ---------  -----  -----  ------  ------
		sample     0.176      0.165  0.189  0.161   0.165   0.143
	match_rate (micro=0.169, macro=0.167):
		  Stub    Start      C      B     GA    FA
		------  -------  -----  -----  -----  ----
		 0.156    0.117  0.247  0.156  0.174  0.15
	filter_rate (micro=0.831, macro=0.833):
		  Stub    Start      C      B     GA    FA
		------  -------  -----  -----  -----  ----
		 0.844    0.883  0.753  0.844  0.826  0.85
	recall (micro=0.579, macro=0.578):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.575    0.333  0.779  0.548  0.507  0.723
	!recall (micro=0.914, macro=0.916):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.933    0.926  0.878  0.919  0.892  0.946
	precision (micro=0.574, macro=0.576):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.648    0.472  0.598  0.563  0.481  0.691
	!precision (micro=0.916, macro=0.917):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.911    0.875  0.944  0.914  0.901  0.953
	f1 (micro=0.572, macro=0.572):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.609    0.391  0.677  0.556  0.494  0.707
	!f1 (micro=0.915, macro=0.916):
		  Stub    Start     C      B     GA    FA
		------  -------  ----  -----  -----  ----
		 0.922      0.9  0.91  0.916  0.897  0.95
	accuracy (micro=0.859, macro=0.86):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		  0.87    0.828  0.859  0.859  0.828  0.914
	fpr (micro=0.086, macro=0.084):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.067    0.074  0.122  0.081  0.108  0.054
	roc_auc (micro=0.896, macro=0.896):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.941    0.855  0.903  0.863  0.855  0.962
	pr_auc (micro=0.595, macro=0.594):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.789    0.439  0.675  0.502  0.417  0.741
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'Stub': {'type': 'number'}, 'Start': {'type': 'number'}, 'C': {'type': 'number'}, 'B': {'type': 'number'}, 'GA': {'type': 'number'}, 'FA': {'type': 'number'}}}}}

