Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18715):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     16911  -->    14183      2728
		False     1804  -->      228      1576
	rates:
		              True    False
		----------  ------  -------
		sample       0.904    0.096
		population   0.11     0.89
	match_rate (micro=0.73, macro=0.5):
		  True    False
		------  -------
		 0.205    0.795
	filter_rate (micro=0.27, macro=0.5):
		  True    False
		------  -------
		 0.795    0.205
	recall (micro=0.87, macro=0.856):
		  True    False
		------  -------
		 0.839    0.874
	!recall (micro=0.843, macro=0.856):
		  True    False
		------  -------
		 0.874    0.839
	precision (micro=0.92, macro=0.715):
		  True    False
		------  -------
		 0.452    0.978
	!precision (micro=0.51, macro=0.715):
		  True    False
		------  -------
		 0.978    0.452
	f1 (micro=0.886, macro=0.755):
		  True    False
		------  -------
		 0.587    0.923
	!f1 (micro=0.624, macro=0.755):
		  True    False
		------  -------
		 0.923    0.587
	accuracy (micro=0.87, macro=0.87):
		  True    False
		------  -------
		  0.87     0.87
	fpr (micro=0.157, macro=0.144):
		  True    False
		------  -------
		 0.126    0.161
	roc_auc (micro=0.935, macro=0.935):
		  True    False
		------  -------
		 0.935    0.935
	pr_auc (micro=0.967, macro=0.889):
		  True    False
		------  -------
		 0.788     0.99
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

