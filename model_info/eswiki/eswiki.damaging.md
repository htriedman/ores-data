Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18715):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      2498  -->     2260       238
		False    16217  -->     3759     12458
	rates:
		              True    False
		----------  ------  -------
		sample       0.133    0.867
		population   0.11     0.89
	match_rate (micro=0.651, macro=0.5):
		  True    False
		------  -------
		 0.306    0.694
	filter_rate (micro=0.349, macro=0.5):
		  True    False
		------  -------
		 0.694    0.306
	recall (micro=0.783, macro=0.836):
		  True    False
		------  -------
		 0.905    0.768
	!recall (micro=0.89, macro=0.836):
		  True    False
		------  -------
		 0.768    0.905
	precision (micro=0.912, macro=0.656):
		  True    False
		------  -------
		 0.326    0.985
	!precision (micro=0.399, macro=0.656):
		  True    False
		------  -------
		 0.985    0.326
	f1 (micro=0.821, macro=0.671):
		  True    False
		------  -------
		  0.48    0.863
	!f1 (micro=0.522, macro=0.671):
		  True    False
		------  -------
		 0.863     0.48
	accuracy (micro=0.783, macro=0.783):
		  True    False
		------  -------
		 0.783    0.783
	fpr (micro=0.11, macro=0.164):
		  True    False
		------  -------
		 0.232    0.095
	roc_auc (micro=0.92, macro=0.92):
		  True    False
		------  -------
		  0.92    0.921
	pr_auc (micro=0.951, macro=0.815):
		  True    False
		------  -------
		 0.641    0.989
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

