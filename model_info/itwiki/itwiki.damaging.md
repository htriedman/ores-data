Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18232):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       712  -->      404       308
		False    17520  -->     1116     16404
	rates:
		              True    False
		----------  ------  -------
		sample       0.039    0.961
		population   0.039    0.961
	match_rate (micro=0.885, macro=0.5):
		  True    False
		------  -------
		 0.083    0.917
	filter_rate (micro=0.115, macro=0.5):
		  True    False
		------  -------
		 0.917    0.083
	recall (micro=0.922, macro=0.752):
		  True    False
		------  -------
		 0.567    0.936
	!recall (micro=0.582, macro=0.752):
		  True    False
		------  -------
		 0.936    0.567
	precision (micro=0.954, macro=0.623):
		  True    False
		------  -------
		 0.264    0.982
	!precision (micro=0.292, macro=0.623):
		  True    False
		------  -------
		 0.982    0.264
	f1 (micro=0.935, macro=0.659):
		  True    False
		------  -------
		  0.36    0.958
	!f1 (micro=0.383, macro=0.659):
		  True    False
		------  -------
		 0.958     0.36
	accuracy (micro=0.922, macro=0.922):
		  True    False
		------  -------
		 0.922    0.922
	fpr (micro=0.418, macro=0.248):
		  True    False
		------  -------
		 0.064    0.433
	roc_auc (micro=0.92, macro=0.921):
		  True    False
		------  -------
		 0.921     0.92
	pr_auc (micro=0.972, macro=0.685):
		  True    False
		------  -------
		 0.373    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

