Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18232):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17879  -->    17720       159
		False      353  -->      224       129
	rates:
		              True    False
		----------  ------  -------
		sample       0.981    0.019
		population   0.981    0.019
	match_rate (micro=0.966, macro=0.5):
		  True    False
		------  -------
		 0.984    0.016
	filter_rate (micro=0.034, macro=0.5):
		  True    False
		------  -------
		 0.016    0.984
	recall (micro=0.979, macro=0.678):
		  True    False
		------  -------
		 0.991    0.365
	!recall (micro=0.377, macro=0.678):
		  True    False
		------  -------
		 0.365    0.991
	precision (micro=0.977, macro=0.717):
		  True    False
		------  -------
		 0.988    0.446
	!precision (micro=0.457, macro=0.717):
		  True    False
		------  -------
		 0.446    0.988
	f1 (micro=0.978, macro=0.696):
		  True    False
		------  -------
		 0.989    0.402
	!f1 (micro=0.413, macro=0.696):
		  True    False
		------  -------
		 0.402    0.989
	accuracy (micro=0.979, macro=0.979):
		  True    False
		------  -------
		 0.979    0.979
	fpr (micro=0.623, macro=0.322):
		  True    False
		------  -------
		 0.635    0.009
	roc_auc (micro=0.938, macro=0.938):
		  True    False
		------  -------
		 0.938    0.939
	pr_auc (micro=0.987, macro=0.699):
		  True    False
		------  -------
		 0.999    0.399
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

