Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=97271):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      2152  -->     1100      1052
		False    95119  -->     4496     90623
	rates:
		              True    False
		----------  ------  -------
		sample       0.022    0.978
		population   0.023    0.977
	match_rate (micro=0.922, macro=0.5):
		  True    False
		------  -------
		 0.058    0.942
	filter_rate (micro=0.078, macro=0.5):
		  True    False
		------  -------
		 0.942    0.058
	recall (micro=0.943, macro=0.732):
		  True    False
		------  -------
		 0.511    0.953
	!recall (micro=0.521, macro=0.732):
		  True    False
		------  -------
		 0.953    0.511
	precision (micro=0.97, macro=0.595):
		  True    False
		------  -------
		 0.201    0.988
	!precision (micro=0.219, macro=0.595):
		  True    False
		------  -------
		 0.988    0.201
	f1 (micro=0.955, macro=0.629):
		  True    False
		------  -------
		 0.288     0.97
	!f1 (micro=0.304, macro=0.629):
		  True    False
		------  -------
		  0.97    0.288
	accuracy (micro=0.943, macro=0.943):
		  True    False
		------  -------
		 0.943    0.943
	fpr (micro=0.479, macro=0.268):
		  True    False
		------  -------
		 0.047    0.489
	roc_auc (micro=0.916, macro=0.916):
		  True    False
		------  -------
		 0.917    0.916
	pr_auc (micro=0.981, macro=0.623):
		  True    False
		------  -------
		 0.248    0.998
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

