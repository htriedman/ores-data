Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=37509):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     37207  -->    36738       469
		False      302  -->      122       180
	rates:
		              True    False
		----------  ------  -------
		sample       0.992    0.008
		population   0.992    0.008
	match_rate (micro=0.975, macro=0.5):
		  True    False
		------  -------
		 0.983    0.017
	filter_rate (micro=0.025, macro=0.5):
		  True    False
		------  -------
		 0.017    0.983
	recall (micro=0.984, macro=0.792):
		  True    False
		------  -------
		 0.987    0.596
	!recall (micro=0.599, macro=0.792):
		  True    False
		------  -------
		 0.596    0.987
	precision (micro=0.991, macro=0.634):
		  True    False
		------  -------
		 0.997    0.271
	!precision (micro=0.276, macro=0.634):
		  True    False
		------  -------
		 0.271    0.997
	f1 (micro=0.987, macro=0.682):
		  True    False
		------  -------
		 0.992    0.372
	!f1 (micro=0.377, macro=0.682):
		  True    False
		------  -------
		 0.372    0.992
	accuracy (micro=0.984, macro=0.984):
		  True    False
		------  -------
		 0.984    0.984
	fpr (micro=0.401, macro=0.208):
		  True    False
		------  -------
		 0.404    0.013
	roc_auc (micro=0.963, macro=0.962):
		  True    False
		------  -------
		 0.963    0.961
	pr_auc (micro=0.995, macro=0.725):
		  True    False
		------  -------
		     1    0.449
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

