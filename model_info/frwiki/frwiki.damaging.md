Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17413):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       562  -->      325       237
		False    16851  -->      666     16185
	rates:
		              True    False
		----------  ------  -------
		sample       0.032    0.968
		population   0.029    0.971
	match_rate (micro=0.919, macro=0.5):
		  True    False
		------  -------
		 0.055    0.945
	filter_rate (micro=0.081, macro=0.5):
		  True    False
		------  -------
		 0.945    0.055
	recall (micro=0.949, macro=0.769):
		  True    False
		------  -------
		 0.578     0.96
	!recall (micro=0.589, macro=0.769):
		  True    False
		------  -------
		  0.96    0.578
	precision (micro=0.967, macro=0.645):
		  True    False
		------  -------
		 0.302    0.987
	!precision (micro=0.322, macro=0.645):
		  True    False
		------  -------
		 0.987    0.302
	f1 (micro=0.957, macro=0.685):
		  True    False
		------  -------
		 0.397    0.974
	!f1 (micro=0.414, macro=0.685):
		  True    False
		------  -------
		 0.974    0.397
	accuracy (micro=0.949, macro=0.949):
		  True    False
		------  -------
		 0.949    0.949
	fpr (micro=0.411, macro=0.231):
		  True    False
		------  -------
		  0.04    0.422
	roc_auc (micro=0.903, macro=0.903):
		  True    False
		------  -------
		 0.903    0.903
	pr_auc (micro=0.978, macro=0.69):
		  True    False
		------  -------
		 0.384    0.996
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

