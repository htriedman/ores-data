Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17413):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17125  -->    16734       391
		False      288  -->      114       174
	rates:
		              True    False
		----------  ------  -------
		sample       0.983    0.017
		population   0.979    0.021
	match_rate (micro=0.945, macro=0.5):
		  True    False
		------  -------
		 0.965    0.035
	filter_rate (micro=0.055, macro=0.5):
		  True    False
		------  -------
		 0.035    0.965
	recall (micro=0.969, macro=0.791):
		  True    False
		------  -------
		 0.977    0.604
	!recall (micro=0.612, macro=0.791):
		  True    False
		------  -------
		 0.604    0.977
	precision (micro=0.978, macro=0.679):
		  True    False
		------  -------
		 0.991    0.366
	!precision (micro=0.38, macro=0.679):
		  True    False
		------  -------
		 0.366    0.991
	f1 (micro=0.973, macro=0.72):
		  True    False
		------  -------
		 0.984    0.456
	!f1 (micro=0.467, macro=0.72):
		  True    False
		------  -------
		 0.456    0.984
	accuracy (micro=0.969, macro=0.969):
		  True    False
		------  -------
		 0.969    0.969
	fpr (micro=0.388, macro=0.209):
		  True    False
		------  -------
		 0.396    0.023
	roc_auc (micro=0.935, macro=0.934):
		  True    False
		------  -------
		 0.935    0.932
	pr_auc (micro=0.987, macro=0.741):
		  True    False
		------  -------
		 0.998    0.483
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

