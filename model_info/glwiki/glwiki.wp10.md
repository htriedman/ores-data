Model Information:
	 - type: GradientBoosting
	 - version: 0.9.0
	 - params: {'scale': True, 'center': True, 'labels': ['Stub', 'Start', 'C', 'B', 'GA', 'FA'], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=401):
		label      n         ~Stub    ~Start    ~C    ~B    ~GA    ~FA
		-------  ---  ---  -------  --------  ----  ----  -----  -----
		'Stub'    67  -->       34        31     2     0      0      0
		'Start'   67  -->       16        30    16     3      2      0
		'C'       67  -->        0        16    26    14      8      3
		'B'       67  -->        1         4    17    20     13     12
		'GA'      66  -->        0         2    10    13     32      9
		'FA'      67  -->        0         2     3    19     17     26
	rates:
		          'Stub'    'Start'    'C'    'B'    'GA'    'FA'
		------  --------  ---------  -----  -----  ------  ------
		sample     0.167      0.167  0.167  0.167   0.165   0.167
	match_rate (micro=0.167, macro=0.167):
		  Stub    Start      C      B    GA     FA
		------  -------  -----  -----  ----  -----
		 0.127    0.212  0.185  0.172  0.18  0.125
	filter_rate (micro=0.833, macro=0.833):
		  Stub    Start      C      B    GA     FA
		------  -------  -----  -----  ----  -----
		 0.873    0.788  0.815  0.828  0.82  0.875
	recall (micro=0.419, macro=0.419):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.507    0.448  0.388  0.299  0.485  0.388
	!recall (micro=0.884, macro=0.884):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.949    0.835  0.856  0.853  0.881  0.928
	precision (micro=0.438, macro=0.438):
		  Stub    Start      C     B     GA    FA
		------  -------  -----  ----  -----  ----
		 0.667    0.353  0.351  0.29  0.444  0.52
	!precision (micro=0.884, macro=0.884):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.906    0.883  0.875  0.858  0.897  0.883
	f1 (micro=0.424, macro=0.424):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.576    0.395  0.369  0.294  0.464  0.444
	!f1 (micro=0.883, macro=0.883):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.927    0.858  0.865  0.856  0.889  0.905
	accuracy (micro=0.806, macro=0.806):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.875    0.771  0.778  0.761  0.815  0.838
	fpr (micro=0.116, macro=0.116):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.051    0.165  0.144  0.147  0.119  0.072
	roc_auc (micro=0.81, macro=0.81):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		  0.95    0.794  0.743  0.705  0.823  0.848
	pr_auc (micro=0.455, macro=0.455):
		  Stub    Start      C      B    GA     FA
		------  -------  -----  -----  ----  -----
		 0.768    0.381  0.315  0.274  0.51  0.485
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'Stub': {'type': 'number'}, 'Start': {'type': 'number'}, 'C': {'type': 'number'}, 'B': {'type': 'number'}, 'GA': {'type': 'number'}, 'FA': {'type': 'number'}}}}}

