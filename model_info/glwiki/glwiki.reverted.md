Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 7, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=59539):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       643  -->      421       222
		False    58896  -->      780     58116
	rates:
		              True    False
		----------  ------  -------
		sample       0.011    0.989
		population   0.04     0.96
	match_rate (micro=0.923, macro=0.5):
		  True    False
		------  -------
		 0.039    0.961
	filter_rate (micro=0.077, macro=0.5):
		  True    False
		------  -------
		 0.961    0.039
	recall (micro=0.973, macro=0.821):
		  True    False
		------  -------
		 0.655    0.987
	!recall (micro=0.668, macro=0.821):
		  True    False
		------  -------
		 0.987    0.655
	precision (micro=0.973, macro=0.831):
		  True    False
		------  -------
		 0.676    0.985
	!precision (micro=0.689, macro=0.831):
		  True    False
		------  -------
		 0.985    0.676
	f1 (micro=0.973, macro=0.826):
		  True    False
		------  -------
		 0.665    0.986
	!f1 (micro=0.678, macro=0.826):
		  True    False
		------  -------
		 0.986    0.665
	accuracy (micro=0.973, macro=0.973):
		  True    False
		------  -------
		 0.973    0.973
	fpr (micro=0.332, macro=0.179):
		  True    False
		------  -------
		 0.013    0.345
	roc_auc (micro=0.948, macro=0.947):
		  True    False
		------  -------
		 0.945    0.949
	pr_auc (micro=0.983, macro=0.821):
		  True    False
		------  -------
		 0.646    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

