Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 3, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=39585):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     39012  -->    38402       610
		False      573  -->      100       473
	rates:
		              True    False
		----------  ------  -------
		sample       0.986    0.014
		population   0.985    0.015
	match_rate (micro=0.959, macro=0.5):
		  True    False
		------  -------
		 0.973    0.027
	filter_rate (micro=0.041, macro=0.5):
		  True    False
		------  -------
		 0.027    0.973
	recall (micro=0.982, macro=0.905):
		  True    False
		------  -------
		 0.984    0.825
	!recall (micro=0.828, macro=0.905):
		  True    False
		------  -------
		 0.825    0.984
	precision (micro=0.989, macro=0.717):
		  True    False
		------  -------
		 0.997    0.437
	!precision (micro=0.445, macro=0.717):
		  True    False
		------  -------
		 0.437    0.997
	f1 (micro=0.985, macro=0.781):
		  True    False
		------  -------
		 0.991    0.572
	!f1 (micro=0.578, macro=0.781):
		  True    False
		------  -------
		 0.572    0.991
	accuracy (micro=0.982, macro=0.982):
		  True    False
		------  -------
		 0.982    0.982
	fpr (micro=0.172, macro=0.095):
		  True    False
		------  -------
		 0.175    0.016
	roc_auc (micro=0.987, macro=0.985):
		  True    False
		------  -------
		 0.987    0.983
	pr_auc (micro=0.996, macro=0.855):
		  True    False
		------  -------
		     1    0.709
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

