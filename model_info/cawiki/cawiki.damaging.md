Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 13, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=39585):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       749  -->      575       174
		False    38836  -->      667     38169
	rates:
		              True    False
		----------  ------  -------
		sample       0.019    0.981
		population   0.019    0.981
	match_rate (micro=0.951, macro=0.5):
		  True    False
		------  -------
		 0.031    0.969
	filter_rate (micro=0.049, macro=0.5):
		  True    False
		------  -------
		 0.969    0.031
	recall (micro=0.979, macro=0.875):
		  True    False
		------  -------
		 0.768    0.983
	!recall (micro=0.772, macro=0.875):
		  True    False
		------  -------
		 0.983    0.768
	precision (micro=0.985, macro=0.73):
		  True    False
		------  -------
		 0.464    0.995
	!precision (micro=0.474, macro=0.73):
		  True    False
		------  -------
		 0.995    0.464
	f1 (micro=0.981, macro=0.784):
		  True    False
		------  -------
		 0.578    0.989
	!f1 (micro=0.586, macro=0.784):
		  True    False
		------  -------
		 0.989    0.578
	accuracy (micro=0.979, macro=0.979):
		  True    False
		------  -------
		 0.979    0.979
	fpr (micro=0.228, macro=0.125):
		  True    False
		------  -------
		 0.017    0.232
	roc_auc (micro=0.977, macro=0.977):
		  True    False
		------  -------
		 0.978    0.977
	pr_auc (micro=0.994, macro=0.842):
		  True    False
		------  -------
		 0.685    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

