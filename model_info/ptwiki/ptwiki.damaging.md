Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=28725):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      2349  -->     1878       471
		False    26376  -->     3875     22501
	rates:
		              True    False
		----------  ------  -------
		sample       0.082    0.918
		population   0.069    0.931
	match_rate (micro=0.766, macro=0.5):
		  True    False
		------  -------
		 0.192    0.808
	filter_rate (micro=0.234, macro=0.5):
		  True    False
		------  -------
		 0.808    0.192
	recall (micro=0.849, macro=0.826):
		  True    False
		------  -------
		 0.799    0.853
	!recall (micro=0.803, macro=0.826):
		  True    False
		------  -------
		 0.853    0.799
	precision (micro=0.935, macro=0.635):
		  True    False
		------  -------
		 0.287    0.983
	!precision (micro=0.335, macro=0.635):
		  True    False
		------  -------
		 0.983    0.287
	f1 (micro=0.88, macro=0.668):
		  True    False
		------  -------
		 0.423    0.913
	!f1 (micro=0.457, macro=0.668):
		  True    False
		------  -------
		 0.913    0.423
	accuracy (micro=0.849, macro=0.849):
		  True    False
		------  -------
		 0.849    0.849
	fpr (micro=0.197, macro=0.174):
		  True    False
		------  -------
		 0.147    0.201
	roc_auc (micro=0.915, macro=0.915):
		  True    False
		------  -------
		 0.916    0.914
	pr_auc (micro=0.96, macro=0.754):
		  True    False
		------  -------
		 0.516    0.993
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

