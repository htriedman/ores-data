Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=28725):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     27001  -->    24616      2385
		False     1724  -->      444      1280
	rates:
		              True    False
		----------  ------  -------
		sample        0.94     0.06
		population    0.94     0.06
	match_rate (micro=0.827, macro=0.5):
		  True    False
		------  -------
		 0.872    0.128
	filter_rate (micro=0.173, macro=0.5):
		  True    False
		------  -------
		 0.128    0.872
	recall (micro=0.901, macro=0.827):
		  True    False
		------  -------
		 0.912    0.742
	!recall (micro=0.753, macro=0.827):
		  True    False
		------  -------
		 0.742    0.912
	precision (micro=0.944, macro=0.666):
		  True    False
		------  -------
		 0.982     0.35
	!precision (micro=0.388, macro=0.666):
		  True    False
		------  -------
		  0.35    0.982
	f1 (micro=0.917, macro=0.711):
		  True    False
		------  -------
		 0.946    0.476
	!f1 (micro=0.504, macro=0.711):
		  True    False
		------  -------
		 0.476    0.946
	accuracy (micro=0.901, macro=0.901):
		  True    False
		------  -------
		 0.901    0.901
	fpr (micro=0.247, macro=0.173):
		  True    False
		------  -------
		 0.258    0.088
	roc_auc (micro=0.932, macro=0.931):
		  True    False
		------  -------
		 0.932    0.931
	pr_auc (micro=0.969, macro=0.775):
		  True    False
		------  -------
		 0.995    0.555
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

