Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19835):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       581  -->      483        98
		False    19254  -->      550     18704
	rates:
		              True    False
		----------  ------  -------
		sample       0.029    0.971
		population   0.029    0.971
	match_rate (micro=0.922, macro=0.5):
		  True    False
		------  -------
		 0.052    0.948
	filter_rate (micro=0.078, macro=0.5):
		  True    False
		------  -------
		 0.948    0.052
	recall (micro=0.967, macro=0.901):
		  True    False
		------  -------
		 0.831    0.971
	!recall (micro=0.835, macro=0.901):
		  True    False
		------  -------
		 0.971    0.831
	precision (micro=0.979, macro=0.731):
		  True    False
		------  -------
		 0.468    0.995
	!precision (micro=0.483, macro=0.731):
		  True    False
		------  -------
		 0.995    0.468
	f1 (micro=0.972, macro=0.791):
		  True    False
		------  -------
		 0.599    0.983
	!f1 (micro=0.61, macro=0.791):
		  True    False
		------  -------
		 0.983    0.599
	accuracy (micro=0.967, macro=0.967):
		  True    False
		------  -------
		 0.967    0.967
	fpr (micro=0.165, macro=0.099):
		  True    False
		------  -------
		 0.029    0.169
	roc_auc (micro=0.981, macro=0.979):
		  True    False
		------  -------
		 0.978    0.981
	pr_auc (micro=0.991, macro=0.856):
		  True    False
		------  -------
		 0.712    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

