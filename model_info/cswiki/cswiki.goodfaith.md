Model Information:
	 - type: GradientBoosting
	 - version: 0.6.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17718):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17516  -->    17350       166
		False      202  -->       95       107
	rates:
		              True    False
		----------  ------  -------
		sample       0.989    0.011
		population   0.978    0.022
	match_rate (micro=0.957, macro=0.5):
		  True    False
		------  -------
		 0.979    0.021
	filter_rate (micro=0.043, macro=0.5):
		  True    False
		------  -------
		 0.021    0.979
	recall (micro=0.98, macro=0.76):
		  True    False
		------  -------
		 0.991     0.53
	!recall (micro=0.54, macro=0.76):
		  True    False
		------  -------
		  0.53    0.991
	precision (micro=0.98, macro=0.776):
		  True    False
		------  -------
		 0.989    0.562
	!precision (micro=0.572, macro=0.776):
		  True    False
		------  -------
		 0.562    0.989
	f1 (micro=0.98, macro=0.768):
		  True    False
		------  -------
		  0.99    0.546
	!f1 (micro=0.556, macro=0.768):
		  True    False
		------  -------
		 0.546     0.99
	accuracy (micro=0.98, macro=0.98):
		  True    False
		------  -------
		  0.98     0.98
	fpr (micro=0.46, macro=0.24):
		  True    False
		------  -------
		  0.47    0.009
	roc_auc (micro=0.962, macro=0.959):
		  True    False
		------  -------
		 0.962    0.957
	pr_auc (micro=0.989, macro=0.794):
		  True    False
		------  -------
		 0.998     0.59
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

