Model Information:
	 - type: GradientBoosting
	 - version: 0.6.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17718):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       450  -->      218       232
		False    17268  -->      357     16911
	rates:
		              True    False
		----------  ------  -------
		sample       0.025    0.975
		population   0.045    0.955
	match_rate (micro=0.918, macro=0.5):
		  True    False
		------  -------
		 0.041    0.959
	filter_rate (micro=0.082, macro=0.5):
		  True    False
		------  -------
		 0.959    0.041
	recall (micro=0.957, macro=0.732):
		  True    False
		------  -------
		 0.484    0.979
	!recall (micro=0.507, macro=0.732):
		  True    False
		------  -------
		 0.979    0.484
	precision (micro=0.956, macro=0.749):
		  True    False
		------  -------
		 0.522    0.976
	!precision (micro=0.543, macro=0.749):
		  True    False
		------  -------
		 0.976    0.522
	f1 (micro=0.956, macro=0.74):
		  True    False
		------  -------
		 0.503    0.978
	!f1 (micro=0.524, macro=0.74):
		  True    False
		------  -------
		 0.978    0.503
	accuracy (micro=0.957, macro=0.957):
		  True    False
		------  -------
		 0.957    0.957
	fpr (micro=0.493, macro=0.268):
		  True    False
		------  -------
		 0.021    0.516
	roc_auc (micro=0.913, macro=0.912):
		  True    False
		------  -------
		 0.911    0.913
	pr_auc (micro=0.973, macro=0.751):
		  True    False
		------  -------
		 0.507    0.994
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

