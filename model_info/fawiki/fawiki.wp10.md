Model Information:
	 - type: GradientBoosting
	 - version: 0.9.0
	 - params: {'scale': True, 'center': True, 'labels': ['Stub', 'Start', 'C', 'B', 'GA', 'FA'], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.5, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 100, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=1229):
		label      n         ~Stub    ~Start    ~C    ~B    ~GA    ~FA
		-------  ---  ---  -------  --------  ----  ----  -----  -----
		'Stub'   224  -->      147        19    32    26      0      0
		'Start'   93  -->       24        23    32    13      0      1
		'C'      150  -->       16        24    56    40     10      4
		'B'      199  -->       13        12    34    60     57     23
		'GA'     300  -->        1         6    10    34    178     71
		'FA'     263  -->        1         0     3    17     85    157
	rates:
		              'Stub'    'Start'    'C'    'B'    'GA'    'FA'
		----------  --------  ---------  -----  -----  ------  ------
		sample         0.182      0.076  0.122  0.162   0.244   0.214
		population     0.182      0.076  0.123  0.162   0.242   0.212
	match_rate (micro=0.186, macro=0.166):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.164    0.068  0.136  0.155  0.268  0.208
	filter_rate (micro=0.814, macro=0.834):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.836    0.932  0.864  0.845  0.732  0.792
	recall (micro=0.505, macro=0.461):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.656    0.247  0.373  0.302  0.593  0.597
	!recall (micro=0.891, macro=0.899):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.945    0.946  0.897  0.874  0.836  0.898
	precision (micro=0.507, macro=0.467):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.727    0.275  0.337  0.315  0.537  0.611
	!precision (micro=0.893, macro=0.9):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.925    0.939  0.911  0.867  0.865  0.892
	f1 (micro=0.505, macro=0.463):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		  0.69     0.26  0.354  0.308  0.564  0.604
	!f1 (micro=0.892, macro=0.9):
		  Stub    Start      C     B     GA     FA
		------  -------  -----  ----  -----  -----
		 0.935    0.942  0.904  0.87  0.851  0.895
	accuracy (micro=0.827, macro=0.835):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.893    0.893  0.833  0.781  0.777  0.834
	fpr (micro=0.109, macro=0.101):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.055    0.054  0.103  0.126  0.164  0.102
	roc_auc (micro=0.805, macro=0.801):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.914    0.804  0.789  0.664  0.794  0.841
	pr_auc (micro=0.491, macro=0.447):
		  Stub    Start      C      B     GA    FA
		------  -------  -----  -----  -----  ----
		 0.761    0.219  0.328  0.263  0.531  0.58
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'Stub': {'type': 'number'}, 'Start': {'type': 'number'}, 'C': {'type': 'number'}, 'B': {'type': 'number'}, 'GA': {'type': 'number'}, 'FA': {'type': 'number'}}}}}

