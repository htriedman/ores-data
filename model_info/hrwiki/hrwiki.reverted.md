Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19347):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1452  -->     1224       228
		False    17895  -->     2483     15412
	rates:
		              True    False
		----------  ------  -------
		sample       0.075    0.925
		population   0.079    0.921
	match_rate (micro=0.757, macro=0.5):
		  True    False
		------  -------
		 0.195    0.805
	filter_rate (micro=0.243, macro=0.5):
		  True    False
		------  -------
		 0.805    0.195
	recall (micro=0.86, macro=0.852):
		  True    False
		------  -------
		 0.843    0.861
	!recall (micro=0.844, macro=0.852):
		  True    False
		------  -------
		 0.861    0.843
	precision (micro=0.934, macro=0.664):
		  True    False
		------  -------
		 0.343    0.985
	!precision (micro=0.394, macro=0.664):
		  True    False
		------  -------
		 0.985    0.343
	f1 (micro=0.885, macro=0.703):
		  True    False
		------  -------
		 0.488    0.919
	!f1 (micro=0.522, macro=0.703):
		  True    False
		------  -------
		 0.919    0.488
	accuracy (micro=0.86, macro=0.86):
		  True    False
		------  -------
		  0.86     0.86
	fpr (micro=0.156, macro=0.148):
		  True    False
		------  -------
		 0.139    0.157
	roc_auc (micro=0.915, macro=0.915):
		  True    False
		------  -------
		 0.916    0.915
	pr_auc (micro=0.954, macro=0.754):
		  True    False
		------  -------
		 0.517    0.992
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

