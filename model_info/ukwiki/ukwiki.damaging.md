Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19129):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       433  -->      267       166
		False    18696  -->      824     17872
	rates:
		              True    False
		----------  ------  -------
		sample       0.023    0.977
		population   0.022    0.978
	match_rate (micro=0.923, macro=0.5):
		  True    False
		------  -------
		 0.057    0.943
	filter_rate (micro=0.077, macro=0.5):
		  True    False
		------  -------
		 0.943    0.057
	recall (micro=0.948, macro=0.786):
		  True    False
		------  -------
		 0.617    0.956
	!recall (micro=0.624, macro=0.786):
		  True    False
		------  -------
		 0.956    0.617
	precision (micro=0.974, macro=0.617):
		  True    False
		------  -------
		 0.243    0.991
	!precision (micro=0.26, macro=0.617):
		  True    False
		------  -------
		 0.991    0.243
	f1 (micro=0.959, macro=0.661):
		  True    False
		------  -------
		 0.349    0.973
	!f1 (micro=0.363, macro=0.661):
		  True    False
		------  -------
		 0.973    0.349
	accuracy (micro=0.948, macro=0.948):
		  True    False
		------  -------
		 0.948    0.948
	fpr (micro=0.376, macro=0.214):
		  True    False
		------  -------
		 0.044    0.383
	roc_auc (micro=0.952, macro=0.952):
		  True    False
		------  -------
		 0.951    0.952
	pr_auc (micro=0.984, macro=0.662):
		  True    False
		------  -------
		 0.325    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

