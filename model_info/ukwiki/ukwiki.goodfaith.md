Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19129):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     18849  -->    18525       324
		False      280  -->      153       127
	rates:
		              True    False
		----------  ------  -------
		sample       0.985    0.015
		population   0.985    0.015
	match_rate (micro=0.962, macro=0.5):
		  True    False
		------  -------
		 0.976    0.024
	filter_rate (micro=0.038, macro=0.5):
		  True    False
		------  -------
		 0.024    0.976
	recall (micro=0.975, macro=0.718):
		  True    False
		------  -------
		 0.983    0.454
	!recall (micro=0.461, macro=0.718):
		  True    False
		------  -------
		 0.454    0.983
	precision (micro=0.981, macro=0.637):
		  True    False
		------  -------
		 0.992    0.282
	!precision (micro=0.293, macro=0.637):
		  True    False
		------  -------
		 0.282    0.992
	f1 (micro=0.978, macro=0.668):
		  True    False
		------  -------
		 0.987    0.348
	!f1 (micro=0.357, macro=0.668):
		  True    False
		------  -------
		 0.348    0.987
	accuracy (micro=0.975, macro=0.975):
		  True    False
		------  -------
		 0.975    0.975
	fpr (micro=0.539, macro=0.282):
		  True    False
		------  -------
		 0.546    0.017
	roc_auc (micro=0.957, macro=0.956):
		  True    False
		------  -------
		 0.957    0.955
	pr_auc (micro=0.989, macro=0.647):
		  True    False
		------  -------
		 0.999    0.295
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

