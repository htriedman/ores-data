Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19524):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       526  -->      349       177
		False    18998  -->      755     18243
	rates:
		              True    False
		----------  ------  -------
		sample       0.027    0.973
		population   0.029    0.971
	match_rate (micro=0.917, macro=0.5):
		  True    False
		------  -------
		 0.058    0.942
	filter_rate (micro=0.083, macro=0.5):
		  True    False
		------  -------
		 0.942    0.058
	recall (micro=0.952, macro=0.812):
		  True    False
		------  -------
		 0.663     0.96
	!recall (micro=0.672, macro=0.812):
		  True    False
		------  -------
		  0.96    0.663
	precision (micro=0.971, macro=0.66):
		  True    False
		------  -------
		  0.33     0.99
	!precision (micro=0.349, macro=0.66):
		  True    False
		------  -------
		  0.99     0.33
	f1 (micro=0.959, macro=0.708):
		  True    False
		------  -------
		 0.441    0.975
	!f1 (micro=0.456, macro=0.708):
		  True    False
		------  -------
		 0.975    0.441
	accuracy (micro=0.952, macro=0.952):
		  True    False
		------  -------
		 0.952    0.952
	fpr (micro=0.328, macro=0.188):
		  True    False
		------  -------
		  0.04    0.337
	roc_auc (micro=0.95, macro=0.95):
		  True    False
		------  -------
		 0.951     0.95
	pr_auc (micro=0.983, macro=0.729):
		  True    False
		------  -------
		  0.46    0.998
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

