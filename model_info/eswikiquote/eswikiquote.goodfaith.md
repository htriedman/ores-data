Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 1, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=8922):
		label       n         ~True    ~False
		-------  ----  ---  -------  --------
		True     8361  -->     8214       147
		False     561  -->      241       320
	rates:
		              True    False
		----------  ------  -------
		sample       0.937    0.063
		population   0.936    0.064
	match_rate (micro=0.889, macro=0.5):
		  True    False
		------  -------
		 0.947    0.053
	filter_rate (micro=0.111, macro=0.5):
		  True    False
		------  -------
		 0.053    0.947
	recall (micro=0.956, macro=0.776):
		  True    False
		------  -------
		 0.982     0.57
	!recall (micro=0.597, macro=0.776):
		  True    False
		------  -------
		  0.57    0.982
	precision (micro=0.953, macro=0.831):
		  True    False
		------  -------
		 0.971    0.691
	!precision (micro=0.709, macro=0.831):
		  True    False
		------  -------
		 0.691    0.971
	f1 (micro=0.954, macro=0.801):
		  True    False
		------  -------
		 0.977    0.625
	!f1 (micro=0.647, macro=0.801):
		  True    False
		------  -------
		 0.625    0.977
	accuracy (micro=0.956, macro=0.956):
		  True    False
		------  -------
		 0.956    0.956
	fpr (micro=0.403, macro=0.224):
		  True    False
		------  -------
		  0.43    0.018
	roc_auc (micro=0.972, macro=0.929):
		  True    False
		------  -------
		 0.978     0.88
	pr_auc (micro=0.963, macro=0.821):
		  True    False
		------  -------
		 0.984    0.658
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

