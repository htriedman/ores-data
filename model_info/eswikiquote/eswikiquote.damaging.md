Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=8922):
		label       n         ~True    ~False
		-------  ----  ---  -------  --------
		True      759  -->      601       158
		False    8163  -->      611      7552
	rates:
		              True    False
		----------  ------  -------
		sample       0.085    0.915
		population   0.087    0.913
	match_rate (micro=0.8, macro=0.5):
		  True    False
		------  -------
		 0.137    0.863
	filter_rate (micro=0.2, macro=0.5):
		  True    False
		------  -------
		 0.863    0.137
	recall (micro=0.914, macro=0.858):
		  True    False
		------  -------
		 0.792    0.925
	!recall (micro=0.803, macro=0.858):
		  True    False
		------  -------
		 0.925    0.792
	precision (micro=0.937, macro=0.741):
		  True    False
		------  -------
		 0.502    0.979
	!precision (micro=0.544, macro=0.741):
		  True    False
		------  -------
		 0.979    0.502
	f1 (micro=0.922, macro=0.783):
		  True    False
		------  -------
		 0.615    0.951
	!f1 (micro=0.644, macro=0.783):
		  True    False
		------  -------
		 0.951    0.615
	accuracy (micro=0.914, macro=0.914):
		  True    False
		------  -------
		 0.914    0.914
	fpr (micro=0.197, macro=0.142):
		  True    False
		------  -------
		 0.075    0.208
	roc_auc (micro=0.948, macro=0.948):
		  True    False
		------  -------
		 0.947    0.948
	pr_auc (micro=0.971, macro=0.861):
		  True    False
		------  -------
		 0.729    0.994
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

