Model Information:
	 - type: RandomForest
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': None, 'criterion': 'entropy', 'max_depth': None, 'max_features': 'log2', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 3, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 320, 'n_jobs': None, 'oob_score': False, 'random_state': None, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=91355):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       704  -->      123       581
		False    90651  -->       14     90637
	rates:
		              True    False
		----------  ------  -------
		sample       0.008    0.992
		population   0.005    0.995
	match_rate (micro=0.994, macro=0.5):
		  True    False
		------  -------
		 0.001    0.999
	filter_rate (micro=0.006, macro=0.5):
		  True    False
		------  -------
		 0.999    0.001
	recall (micro=0.996, macro=0.587):
		  True    False
		------  -------
		 0.175        1
	!recall (micro=0.179, macro=0.587):
		  True    False
		------  -------
		     1    0.175
	precision (micro=0.995, macro=0.92):
		  True    False
		------  -------
		 0.845    0.996
	!precision (micro=0.845, macro=0.92):
		  True    False
		------  -------
		 0.996    0.845
	f1 (micro=0.995, macro=0.644):
		  True    False
		------  -------
		  0.29    0.998
	!f1 (micro=0.293, macro=0.644):
		  True    False
		------  -------
		 0.998     0.29
	accuracy (micro=0.996, macro=0.996):
		  True    False
		------  -------
		 0.996    0.996
	fpr (micro=0.821, macro=0.413):
		  True    False
		------  -------
		     0    0.825
	roc_auc (micro=0.974, macro=0.97):
		  True    False
		------  -------
		 0.966    0.974
	pr_auc (micro=0.997, macro=0.73):
		  True    False
		------  -------
		  0.46        1
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

