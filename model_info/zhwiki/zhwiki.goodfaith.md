Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 5, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=68782):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     68519  -->    68106       413
		False      263  -->      190        73
	rates:
		              True    False
		----------  ------  -------
		sample       0.996    0.004
		population   0.968    0.032
	match_rate (micro=0.954, macro=0.5):
		  True    False
		------  -------
		 0.985    0.015
	filter_rate (micro=0.046, macro=0.5):
		  True    False
		------  -------
		 0.015    0.985
	recall (micro=0.971, macro=0.636):
		  True    False
		------  -------
		 0.994    0.278
	!recall (micro=0.3, macro=0.636):
		  True    False
		------  -------
		 0.278    0.994
	precision (micro=0.965, macro=0.789):
		  True    False
		------  -------
		 0.977    0.602
	!precision (micro=0.614, macro=0.789):
		  True    False
		------  -------
		 0.602    0.977
	f1 (micro=0.966, macro=0.683):
		  True    False
		------  -------
		 0.985     0.38
	!f1 (micro=0.399, macro=0.683):
		  True    False
		------  -------
		  0.38    0.985
	accuracy (micro=0.971, macro=0.971):
		  True    False
		------  -------
		 0.971    0.971
	fpr (micro=0.7, macro=0.364):
		  True    False
		------  -------
		 0.722    0.006
	roc_auc (micro=0.893, macro=0.892):
		  True    False
		------  -------
		 0.893    0.892
	pr_auc (micro=0.978, macro=0.72):
		  True    False
		------  -------
		 0.995    0.444
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

