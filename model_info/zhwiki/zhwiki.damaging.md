Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 7, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=68782):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       352  -->      151       201
		False    68430  -->      765     67665
	rates:
		              True    False
		----------  ------  -------
		sample       0.005    0.995
		population   0.04     0.96
	match_rate (micro=0.934, macro=0.5):
		  True    False
		------  -------
		 0.028    0.972
	filter_rate (micro=0.066, macro=0.5):
		  True    False
		------  -------
		 0.972    0.028
	recall (micro=0.966, macro=0.709):
		  True    False
		------  -------
		 0.429    0.989
	!recall (micro=0.452, macro=0.709):
		  True    False
		------  -------
		 0.989    0.429
	precision (micro=0.962, macro=0.797):
		  True    False
		------  -------
		 0.618    0.976
	!precision (micro=0.633, macro=0.797):
		  True    False
		------  -------
		 0.976    0.618
	f1 (micro=0.963, macro=0.744):
		  True    False
		------  -------
		 0.507    0.982
	!f1 (micro=0.526, macro=0.744):
		  True    False
		------  -------
		 0.982    0.507
	accuracy (micro=0.966, macro=0.966):
		  True    False
		------  -------
		 0.966    0.966
	fpr (micro=0.548, macro=0.291):
		  True    False
		------  -------
		 0.011    0.571
	roc_auc (micro=0.877, macro=0.877):
		  True    False
		------  -------
		 0.877    0.877
	pr_auc (micro=0.971, macro=0.73):
		  True    False
		------  -------
		 0.467    0.992
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

