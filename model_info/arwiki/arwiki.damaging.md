Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 100, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18208):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       336  -->        0       336
		False    17872  -->        2     17870
	rates:
		              True    False
		----------  ------  -------
		sample       0.018    0.982
		population   0.021    0.979
	match_rate (micro=0.978, macro=0.5):
		  True    False
		------  -------
		     0        1
	filter_rate (micro=0.022, macro=0.5):
		  True    False
		------  -------
		     1        0
	recall (micro=0.978, macro=0.5):
		  True    False
		------  -------
		     0        1
	!recall (micro=0.021, macro=0.5):
		  True    False
		------  -------
		     1        0
	precision (micro=0.958, macro=0.489):
		  True    False
		------  -------
		     0    0.979
	!precision (micro=0.021, macro=0.489):
		  True    False
		------  -------
		 0.979        0
	f1 (micro=None, macro=None):
		True      False
		------  -------
		   NaN    0.989
	!f1 (micro=None, macro=None):
		  True  False
		------  -------
		 0.989
	accuracy (micro=0.978, macro=0.978):
		  True    False
		------  -------
		 0.978    0.978
	fpr (micro=0.979, macro=0.5):
		  True    False
		------  -------
		     0        1
	roc_auc (micro=0.937, macro=0.937):
		  True    False
		------  -------
		 0.937    0.937
	pr_auc (micro=0.982, macro=0.63):
		  True    False
		------  -------
		 0.262    0.998
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

