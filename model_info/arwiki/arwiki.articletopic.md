Model Information:
	 - type: GradientBoosting
	 - version: 1.4.0
	 - params: {'scale': False, 'center': False, 'labels': ['Culture.Biography.Biography*', 'Culture.Biography.Women', 'Culture.Food and drink', 'Culture.Internet culture', 'Culture.Linguistics', 'Culture.Literature', 'Culture.Media.Books', 'Culture.Media.Entertainment', 'Culture.Media.Films', 'Culture.Media.Media*', 'Culture.Media.Music', 'Culture.Media.Radio', 'Culture.Media.Software', 'Culture.Media.Television', 'Culture.Media.Video games', 'Culture.Performing arts', 'Culture.Philosophy and religion', 'Culture.Sports', 'Culture.Visual arts.Architecture', 'Culture.Visual arts.Comics and Anime', 'Culture.Visual arts.Fashion', 'Culture.Visual arts.Visual arts*', 'Geography.Geographical', 'Geography.Regions.Africa.Africa*', 'Geography.Regions.Africa.Central Africa', 'Geography.Regions.Africa.Eastern Africa', 'Geography.Regions.Africa.Northern Africa', 'Geography.Regions.Africa.Southern Africa', 'Geography.Regions.Africa.Western Africa', 'Geography.Regions.Americas.Central America', 'Geography.Regions.Americas.North America', 'Geography.Regions.Americas.South America', 'Geography.Regions.Asia.Asia*', 'Geography.Regions.Asia.Central Asia', 'Geography.Regions.Asia.East Asia', 'Geography.Regions.Asia.North Asia', 'Geography.Regions.Asia.South Asia', 'Geography.Regions.Asia.Southeast Asia', 'Geography.Regions.Asia.West Asia', 'Geography.Regions.Europe.Eastern Europe', 'Geography.Regions.Europe.Europe*', 'Geography.Regions.Europe.Northern Europe', 'Geography.Regions.Europe.Southern Europe', 'Geography.Regions.Europe.Western Europe', 'Geography.Regions.Oceania', 'History and Society.Business and economics', 'History and Society.Education', 'History and Society.History', 'History and Society.Military and warfare', 'History and Society.Politics and government', 'History and Society.Society', 'History and Society.Transportation', 'STEM.Biology', 'STEM.Chemistry', 'STEM.Computing', 'STEM.Earth and environment', 'STEM.Engineering', 'STEM.Libraries & Information', 'STEM.Mathematics', 'STEM.Medicine & Health', 'STEM.Physics', 'STEM.STEM*', 'STEM.Space', 'STEM.Technology'], 'multilabel': True, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 150, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': {}}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=63364):
			label                                              n          TP    FP    FN     TN
			---------------------------------------------  -----  ---  -----  ----  ----  -----
			'Culture.Biography.Biography*'                 27484  -->  26431  1053   865  35015
			'Culture.Biography.Women'                       6106  -->   4346  1760   917  56341
			'Culture.Food and drink'                        1375  -->    870   505   102  61887
			'Culture.Internet culture'                      3354  -->   2676   678   226  59784
			'Culture.Linguistics'                           1435  -->    911   524    91  61838
			'Culture.Literature'                            5701  -->   4106  1595   596  57067
			'Culture.Media.Books'                           1581  -->   1176   405   129  61654
			'Culture.Media.Entertainment'                   2112  -->    980  1132   223  61029
			'Culture.Media.Films'                           2053  -->   1574   479   115  61196
			'Culture.Media.Media*'                         14001  -->  11521  2480  1662  47701
			'Culture.Media.Music'                           2678  -->   1991   687   329  60357
			'Culture.Media.Radio'                           1123  -->    463   660    96  62145
			'Culture.Media.Software'                        1989  -->   1402   587   319  61056
			'Culture.Media.Television'                      2317  -->   1324   993   259  60788
			'Culture.Media.Video games'                     2203  -->   1973   230    62  61099
			'Culture.Performing arts'                       1539  -->    735   804   155  61670
			'Culture.Philosophy and religion'               3317  -->   1609  1708   357  59690
			'Culture.Sports'                                8914  -->   8162   752   385  54065
			'Culture.Visual arts.Architecture'              1813  -->   1141   672   186  61365
			'Culture.Visual arts.Comics and Anime'          1876  -->   1456   420   101  61387
			'Culture.Visual arts.Fashion'                   1360  -->   1005   355   109  61895
			'Culture.Visual arts.Visual arts*'              5891  -->   4148  1743   443  57030
			'Geography.Geographical'                        3687  -->   2614  1073   421  59256
			'Geography.Regions.Africa.Africa*'              6437  -->   5143  1294   431  56496
			'Geography.Regions.Africa.Central Africa'       1126  -->    837   289    50  62188
			'Geography.Regions.Africa.Eastern Africa'        968  -->    660   308    60  62336
			'Geography.Regions.Africa.Northern Africa'      2008  -->   1465   543   213  61143
			'Geography.Regions.Africa.Southern Africa'      1196  -->    912   284    49  62119
			'Geography.Regions.Africa.Western Africa'        721  -->    495   226    44  62599
			'Geography.Regions.Americas.Central America'    1356  -->    837   519    75  61933
			'Geography.Regions.Americas.North America'      8321  -->   5547  2774  2239  52804
			'Geography.Regions.Americas.South America'      1643  -->   1230   413   103  61618
			'Geography.Regions.Asia.Asia*'                 12468  -->  10325  2143   902  49994
			'Geography.Regions.Asia.Central Asia'           1166  -->    808   358    69  62129
			'Geography.Regions.Asia.East Asia'              2811  -->   2022   789   268  60285
			'Geography.Regions.Asia.North Asia'             2444  -->   1725   719   217  60703
			'Geography.Regions.Asia.South Asia'             1839  -->   1402   437    60  61465
			'Geography.Regions.Asia.Southeast Asia'         1594  -->   1142   452    65  61705
			'Geography.Regions.Asia.West Asia'              3729  -->   2967   762   296  59339
			'Geography.Regions.Europe.Eastern Europe'       3523  -->   2569   954   241  59600
			'Geography.Regions.Europe.Europe*'             13759  -->  10696  3063  2025  47580
			'Geography.Regions.Europe.Northern Europe'      4058  -->   2640  1418   635  58671
			'Geography.Regions.Europe.Southern Europe'      2814  -->   2020   794   294  60256
			'Geography.Regions.Europe.Western Europe'       3997  -->   2921  1076   584  58783
			'Geography.Regions.Oceania'                     2313  -->   1864   449    81  60970
			'History and Society.Business and economics'    3556  -->   1924  1632   473  59335
			'History and Society.Education'                 1988  -->    854  1134   188  61188
			'History and Society.History'                   4321  -->   1808  2513   642  58401
			'History and Society.Military and warfare'      3987  -->   2540  1447   412  58965
			'History and Society.Politics and government'   5373  -->   3181  2192   663  57328
			'History and Society.Society'                   4393  -->   1070  3323   363  58608
			'History and Society.Transportation'            3249  -->   2777   472   153  59962
			'STEM.Biology'                                  3007  -->   2282   725   212  60145
			'STEM.Chemistry'                                1472  -->   1003   469   159  61733
			'STEM.Computing'                                2390  -->   1775   615   390  60584
			'STEM.Earth and environment'                    1693  -->    962   731   178  61493
			'STEM.Engineering'                              2891  -->   2054   837   240  60233
			'STEM.Libraries & Information'                  1184  -->    748   436    60  62120
			'STEM.Mathematics'                              1144  -->    739   405    63  62157
			'STEM.Medicine & Health'                        2210  -->   1493   717   226  60928
			'STEM.Physics'                                  1446  -->    885   561   206  61712
			'STEM.STEM*'                                   17887  -->  15502  2385  1108  44369
			'STEM.Space'                                    1720  -->   1499   221    56  61588
			'STEM.Technology'                               4341  -->   2892  1449   607  58416
	rates:
		                                               sample    population
		-------------------------------------------  --------  ------------
		Culture.Biography.Biography*                    0.434         0.12
		Culture.Biography.Women                         0.096         0.015
		Culture.Food and drink                          0.022         0.003
		Culture.Internet culture                        0.053         0.004
		Culture.Linguistics                             0.023         0.008
		Culture.Literature                              0.09          0.015
		Culture.Media.Books                             0.025         0.004
		Culture.Media.Entertainment                     0.033         0.004
		Culture.Media.Films                             0.032         0.012
		Culture.Media.Media*                            0.221         0.055
		Culture.Media.Music                             0.042         0.021
		Culture.Media.Radio                             0.018         0.002
		Culture.Media.Software                          0.031         0.001
		Culture.Media.Television                        0.037         0.009
		Culture.Media.Video games                       0.035         0.003
		Culture.Performing arts                         0.024         0.003
		Culture.Philosophy and religion                 0.052         0.01
		Culture.Sports                                  0.141         0.06
		Culture.Visual arts.Architecture                0.029         0.011
		Culture.Visual arts.Comics and Anime            0.03          0.002
		Culture.Visual arts.Fashion                     0.021         0.001
		Culture.Visual arts.Visual arts*                0.093         0.018
		Geography.Geographical                          0.058         0.021
		Geography.Regions.Africa.Africa*                0.102         0.008
		Geography.Regions.Africa.Central Africa         0.018         0.001
		Geography.Regions.Africa.Eastern Africa         0.015         0.001
		Geography.Regions.Africa.Northern Africa        0.032         0.001
		Geography.Regions.Africa.Southern Africa        0.019         0.001
		Geography.Regions.Africa.Western Africa         0.011         0.001
		Geography.Regions.Americas.Central America      0.021         0.003
		Geography.Regions.Americas.North America        0.131         0.063
		Geography.Regions.Americas.South America        0.026         0.007
		Geography.Regions.Asia.Asia*                    0.197         0.052
		Geography.Regions.Asia.Central Asia             0.018         0.001
		Geography.Regions.Asia.East Asia                0.044         0.012
		Geography.Regions.Asia.North Asia               0.039         0.006
		Geography.Regions.Asia.South Asia               0.029         0.016
		Geography.Regions.Asia.Southeast Asia           0.025         0.006
		Geography.Regions.Asia.West Asia                0.059         0.012
		Geography.Regions.Europe.Eastern Europe         0.056         0.018
		Geography.Regions.Europe.Europe*                0.217         0.081
		Geography.Regions.Europe.Northern Europe        0.064         0.029
		Geography.Regions.Europe.Southern Europe        0.044         0.014
		Geography.Regions.Europe.Western Europe         0.063         0.02
		Geography.Regions.Oceania                       0.037         0.016
		History and Society.Business and economics      0.056         0.01
		History and Society.Education                   0.031         0.008
		History and Society.History                     0.068         0.011
		History and Society.Military and warfare        0.063         0.015
		History and Society.Politics and government     0.085         0.028
		History and Society.Society                     0.069         0.008
		History and Society.Transportation              0.051         0.016
		STEM.Biology                                    0.047         0.034
		STEM.Chemistry                                  0.023         0.002
		STEM.Computing                                  0.038         0.003
		STEM.Earth and environment                      0.027         0.005
		STEM.Engineering                                0.046         0.006
		STEM.Libraries & Information                    0.019         0.001
		STEM.Mathematics                                0.018         0
		STEM.Medicine & Health                          0.035         0.006
		STEM.Physics                                    0.023         0.001
		STEM.STEM*                                      0.282         0.065
		STEM.Space                                      0.027         0.004
		STEM.Technology                                 0.069         0.005
	match_rate (micro=0.054, macro=0.018):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.137
		Culture.Biography.Women                      0.026
		Culture.Food and drink                       0.003
		Culture.Internet culture                     0.007
		Culture.Linguistics                          0.007
		Culture.Literature                           0.021
		Culture.Media.Books                          0.005
		Culture.Media.Entertainment                  0.005
		Culture.Media.Films                          0.011
		Culture.Media.Media*                         0.077
		Culture.Media.Music                          0.021
		Culture.Media.Radio                          0.002
		Culture.Media.Software                       0.006
		Culture.Media.Television                     0.009
		Culture.Media.Video games                    0.004
		Culture.Performing arts                      0.004
		Culture.Philosophy and religion              0.011
		Culture.Sports                               0.062
		Culture.Visual arts.Architecture             0.01
		Culture.Visual arts.Comics and Anime         0.003
		Culture.Visual arts.Fashion                  0.002
		Culture.Visual arts.Visual arts*             0.02
		Geography.Geographical                       0.022
		Geography.Regions.Africa.Africa*             0.014
		Geography.Regions.Africa.Central Africa      0.001
		Geography.Regions.Africa.Eastern Africa      0.001
		Geography.Regions.Africa.Northern Africa     0.004
		Geography.Regions.Africa.Southern Africa     0.002
		Geography.Regions.Africa.Western Africa      0.001
		Geography.Regions.Americas.Central America   0.003
		Geography.Regions.Americas.North America     0.08
		Geography.Regions.Americas.South America     0.007
		Geography.Regions.Asia.Asia*                 0.06
		Geography.Regions.Asia.Central Asia          0.002
		Geography.Regions.Asia.East Asia             0.013
		Geography.Regions.Asia.North Asia            0.007
		Geography.Regions.Asia.South Asia            0.014
		Geography.Regions.Asia.Southeast Asia        0.006
		Geography.Regions.Asia.West Asia             0.014
		Geography.Regions.Europe.Eastern Europe      0.017
		Geography.Regions.Europe.Europe*             0.1
		Geography.Regions.Europe.Northern Europe     0.029
		Geography.Regions.Europe.Southern Europe     0.015
		Geography.Regions.Europe.Western Europe      0.025
		Geography.Regions.Oceania                    0.015
		History and Society.Business and economics   0.013
		History and Society.Education                0.006
		History and Society.History                  0.015
		History and Society.Military and warfare     0.017
		History and Society.Politics and government  0.028
		History and Society.Society                  0.008
		History and Society.Transportation           0.016
		STEM.Biology                                 0.03
		STEM.Chemistry                               0.004
		STEM.Computing                               0.008
		STEM.Earth and environment                   0.006
		STEM.Engineering                             0.008
		STEM.Libraries & Information                 0.001
		STEM.Mathematics                             0.001
		STEM.Medicine & Health                       0.008
		STEM.Physics                                 0.004
		STEM.STEM*                                   0.079
		STEM.Space                                   0.005
		STEM.Technology                              0.014
		-------------------------------------------  -----
	filter_rate (micro=0.946, macro=0.982):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.863
		Culture.Biography.Women                      0.974
		Culture.Food and drink                       0.997
		Culture.Internet culture                     0.993
		Culture.Linguistics                          0.993
		Culture.Literature                           0.979
		Culture.Media.Books                          0.995
		Culture.Media.Entertainment                  0.995
		Culture.Media.Films                          0.989
		Culture.Media.Media*                         0.923
		Culture.Media.Music                          0.979
		Culture.Media.Radio                          0.998
		Culture.Media.Software                       0.994
		Culture.Media.Television                     0.991
		Culture.Media.Video games                    0.996
		Culture.Performing arts                      0.996
		Culture.Philosophy and religion              0.989
		Culture.Sports                               0.938
		Culture.Visual arts.Architecture             0.99
		Culture.Visual arts.Comics and Anime         0.997
		Culture.Visual arts.Fashion                  0.998
		Culture.Visual arts.Visual arts*             0.98
		Geography.Geographical                       0.978
		Geography.Regions.Africa.Africa*             0.986
		Geography.Regions.Africa.Central Africa      0.999
		Geography.Regions.Africa.Eastern Africa      0.999
		Geography.Regions.Africa.Northern Africa     0.996
		Geography.Regions.Africa.Southern Africa     0.998
		Geography.Regions.Africa.Western Africa      0.999
		Geography.Regions.Americas.Central America   0.997
		Geography.Regions.Americas.North America     0.92
		Geography.Regions.Americas.South America     0.993
		Geography.Regions.Asia.Asia*                 0.94
		Geography.Regions.Asia.Central Asia          0.998
		Geography.Regions.Asia.East Asia             0.987
		Geography.Regions.Asia.North Asia            0.993
		Geography.Regions.Asia.South Asia            0.986
		Geography.Regions.Asia.Southeast Asia        0.994
		Geography.Regions.Asia.West Asia             0.986
		Geography.Regions.Europe.Eastern Europe      0.983
		Geography.Regions.Europe.Europe*             0.9
		Geography.Regions.Europe.Northern Europe     0.971
		Geography.Regions.Europe.Southern Europe     0.985
		Geography.Regions.Europe.Western Europe      0.975
		Geography.Regions.Oceania                    0.985
		History and Society.Business and economics   0.987
		History and Society.Education                0.994
		History and Society.History                  0.985
		History and Society.Military and warfare     0.983
		History and Society.Politics and government  0.972
		History and Society.Society                  0.992
		History and Society.Transportation           0.984
		STEM.Biology                                 0.97
		STEM.Chemistry                               0.996
		STEM.Computing                               0.992
		STEM.Earth and environment                   0.994
		STEM.Engineering                             0.992
		STEM.Libraries & Information                 0.999
		STEM.Mathematics                             0.999
		STEM.Medicine & Health                       0.992
		STEM.Physics                                 0.996
		STEM.STEM*                                   0.921
		STEM.Space                                   0.995
		STEM.Technology                              0.986
		-------------------------------------------  -----
	recall (micro=0.765, macro=0.691):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.962
		Culture.Biography.Women                      0.712
		Culture.Food and drink                       0.633
		Culture.Internet culture                     0.798
		Culture.Linguistics                          0.635
		Culture.Literature                           0.72
		Culture.Media.Books                          0.744
		Culture.Media.Entertainment                  0.464
		Culture.Media.Films                          0.767
		Culture.Media.Media*                         0.823
		Culture.Media.Music                          0.743
		Culture.Media.Radio                          0.412
		Culture.Media.Software                       0.705
		Culture.Media.Television                     0.571
		Culture.Media.Video games                    0.896
		Culture.Performing arts                      0.478
		Culture.Philosophy and religion              0.485
		Culture.Sports                               0.916
		Culture.Visual arts.Architecture             0.629
		Culture.Visual arts.Comics and Anime         0.776
		Culture.Visual arts.Fashion                  0.739
		Culture.Visual arts.Visual arts*             0.704
		Geography.Geographical                       0.709
		Geography.Regions.Africa.Africa*             0.799
		Geography.Regions.Africa.Central Africa      0.743
		Geography.Regions.Africa.Eastern Africa      0.682
		Geography.Regions.Africa.Northern Africa     0.73
		Geography.Regions.Africa.Southern Africa     0.763
		Geography.Regions.Africa.Western Africa      0.687
		Geography.Regions.Americas.Central America   0.617
		Geography.Regions.Americas.North America     0.667
		Geography.Regions.Americas.South America     0.749
		Geography.Regions.Asia.Asia*                 0.828
		Geography.Regions.Asia.Central Asia          0.693
		Geography.Regions.Asia.East Asia             0.719
		Geography.Regions.Asia.North Asia            0.706
		Geography.Regions.Asia.South Asia            0.762
		Geography.Regions.Asia.Southeast Asia        0.716
		Geography.Regions.Asia.West Asia             0.796
		Geography.Regions.Europe.Eastern Europe      0.729
		Geography.Regions.Europe.Europe*             0.777
		Geography.Regions.Europe.Northern Europe     0.651
		Geography.Regions.Europe.Southern Europe     0.718
		Geography.Regions.Europe.Western Europe      0.731
		Geography.Regions.Oceania                    0.806
		History and Society.Business and economics   0.541
		History and Society.Education                0.43
		History and Society.History                  0.418
		History and Society.Military and warfare     0.637
		History and Society.Politics and government  0.592
		History and Society.Society                  0.244
		History and Society.Transportation           0.855
		STEM.Biology                                 0.759
		STEM.Chemistry                               0.681
		STEM.Computing                               0.743
		STEM.Earth and environment                   0.568
		STEM.Engineering                             0.71
		STEM.Libraries & Information                 0.632
		STEM.Mathematics                             0.646
		STEM.Medicine & Health                       0.676
		STEM.Physics                                 0.612
		STEM.STEM*                                   0.867
		STEM.Space                                   0.872
		STEM.Technology                              0.666
		-------------------------------------------  -----
	!recall (micro=0.984, macro=0.993):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.976
		Culture.Biography.Women                      0.984
		Culture.Food and drink                       0.998
		Culture.Internet culture                     0.996
		Culture.Linguistics                          0.999
		Culture.Literature                           0.99
		Culture.Media.Books                          0.998
		Culture.Media.Entertainment                  0.996
		Culture.Media.Films                          0.998
		Culture.Media.Media*                         0.966
		Culture.Media.Music                          0.995
		Culture.Media.Radio                          0.998
		Culture.Media.Software                       0.995
		Culture.Media.Television                     0.996
		Culture.Media.Video games                    0.999
		Culture.Performing arts                      0.997
		Culture.Philosophy and religion              0.994
		Culture.Sports                               0.993
		Culture.Visual arts.Architecture             0.997
		Culture.Visual arts.Comics and Anime         0.998
		Culture.Visual arts.Fashion                  0.998
		Culture.Visual arts.Visual arts*             0.992
		Geography.Geographical                       0.993
		Geography.Regions.Africa.Africa*             0.992
		Geography.Regions.Africa.Central Africa      0.999
		Geography.Regions.Africa.Eastern Africa      0.999
		Geography.Regions.Africa.Northern Africa     0.997
		Geography.Regions.Africa.Southern Africa     0.999
		Geography.Regions.Africa.Western Africa      0.999
		Geography.Regions.Americas.Central America   0.999
		Geography.Regions.Americas.North America     0.959
		Geography.Regions.Americas.South America     0.998
		Geography.Regions.Asia.Asia*                 0.982
		Geography.Regions.Asia.Central Asia          0.999
		Geography.Regions.Asia.East Asia             0.996
		Geography.Regions.Asia.North Asia            0.996
		Geography.Regions.Asia.South Asia            0.999
		Geography.Regions.Asia.Southeast Asia        0.999
		Geography.Regions.Asia.West Asia             0.995
		Geography.Regions.Europe.Eastern Europe      0.996
		Geography.Regions.Europe.Europe*             0.959
		Geography.Regions.Europe.Northern Europe     0.989
		Geography.Regions.Europe.Southern Europe     0.995
		Geography.Regions.Europe.Western Europe      0.99
		Geography.Regions.Oceania                    0.999
		History and Society.Business and economics   0.992
		History and Society.Education                0.997
		History and Society.History                  0.989
		History and Society.Military and warfare     0.993
		History and Society.Politics and government  0.989
		History and Society.Society                  0.994
		History and Society.Transportation           0.997
		STEM.Biology                                 0.996
		STEM.Chemistry                               0.997
		STEM.Computing                               0.994
		STEM.Earth and environment                   0.997
		STEM.Engineering                             0.996
		STEM.Libraries & Information                 0.999
		STEM.Mathematics                             0.999
		STEM.Medicine & Health                       0.996
		STEM.Physics                                 0.997
		STEM.STEM*                                   0.976
		STEM.Space                                   0.999
		STEM.Technology                              0.99
		-------------------------------------------  -----
	precision (micro=0.675, macro=0.546):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.845
		Culture.Biography.Women                      0.403
		Culture.Food and drink                       0.495
		Culture.Internet culture                     0.442
		Culture.Linguistics                          0.777
		Culture.Literature                           0.512
		Culture.Media.Books                          0.607
		Culture.Media.Entertainment                  0.335
		Culture.Media.Films                          0.826
		Culture.Media.Media*                         0.586
		Culture.Media.Music                          0.746
		Culture.Media.Radio                          0.383
		Culture.Media.Software                       0.151
		Culture.Media.Television                     0.546
		Culture.Media.Video games                    0.72
		Culture.Performing arts                      0.368
		Culture.Philosophy and religion              0.46
		Culture.Sports                               0.892
		Culture.Visual arts.Architecture             0.694
		Culture.Visual arts.Comics and Anime         0.531
		Culture.Visual arts.Fashion                  0.273
		Culture.Visual arts.Visual arts*             0.627
		Geography.Geographical                       0.684
		Geography.Regions.Africa.Africa*             0.474
		Geography.Regions.Africa.Central Africa      0.394
		Geography.Regions.Africa.Eastern Africa      0.262
		Geography.Regions.Africa.Northern Africa     0.221
		Geography.Regions.Africa.Southern Africa     0.558
		Geography.Regions.Africa.Western Africa      0.421
		Geography.Regions.Americas.Central America   0.638
		Geography.Regions.Americas.North America     0.524
		Geography.Regions.Americas.South America     0.755
		Geography.Regions.Asia.Asia*                 0.721
		Geography.Regions.Asia.Central Asia          0.332
		Geography.Regions.Asia.East Asia             0.666
		Geography.Regions.Asia.North Asia            0.527
		Geography.Regions.Asia.South Asia            0.929
		Geography.Regions.Asia.Southeast Asia        0.81
		Geography.Regions.Asia.West Asia             0.654
		Geography.Regions.Europe.Eastern Europe      0.771
		Geography.Regions.Europe.Europe*             0.626
		Geography.Regions.Europe.Northern Europe     0.644
		Geography.Regions.Europe.Southern Europe     0.674
		Geography.Regions.Europe.Western Europe      0.608
		Geography.Regions.Oceania                    0.91
		History and Society.Business and economics   0.401
		History and Society.Education                0.528
		History and Society.History                  0.297
		History and Society.Military and warfare     0.587
		History and Society.Politics and government  0.598
		History and Society.Society                  0.248
		History and Society.Transportation           0.847
		STEM.Biology                                 0.885
		STEM.Chemistry                               0.31
		STEM.Computing                               0.247
		STEM.Earth and environment                   0.483
		STEM.Engineering                             0.508
		STEM.Libraries & Information                 0.309
		STEM.Mathematics                             0.227
		STEM.Medicine & Health                       0.542
		STEM.Physics                                 0.147
		STEM.STEM*                                   0.712
		STEM.Space                                   0.804
		STEM.Technology                              0.251
		-------------------------------------------  -----
	!precision (micro=0.992, macro=0.996):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.995
		Culture.Biography.Women                      0.996
		Culture.Food and drink                       0.999
		Culture.Internet culture                     0.999
		Culture.Linguistics                          0.997
		Culture.Literature                           0.996
		Culture.Media.Books                          0.999
		Culture.Media.Entertainment                  0.998
		Culture.Media.Films                          0.997
		Culture.Media.Media*                         0.989
		Culture.Media.Music                          0.995
		Culture.Media.Radio                          0.999
		Culture.Media.Software                       1
		Culture.Media.Television                     0.996
		Culture.Media.Video games                    1
		Culture.Performing arts                      0.998
		Culture.Philosophy and religion              0.995
		Culture.Sports                               0.995
		Culture.Visual arts.Architecture             0.996
		Culture.Visual arts.Comics and Anime         0.999
		Culture.Visual arts.Fashion                  1
		Culture.Visual arts.Visual arts*             0.995
		Geography.Geographical                       0.994
		Geography.Regions.Africa.Africa*             0.998
		Geography.Regions.Africa.Central Africa      1
		Geography.Regions.Africa.Eastern Africa      1
		Geography.Regions.Africa.Northern Africa     1
		Geography.Regions.Africa.Southern Africa     1
		Geography.Regions.Africa.Western Africa      1
		Geography.Regions.Americas.Central America   0.999
		Geography.Regions.Americas.North America     0.977
		Geography.Regions.Americas.South America     0.998
		Geography.Regions.Asia.Asia*                 0.99
		Geography.Regions.Asia.Central Asia          1
		Geography.Regions.Asia.East Asia             0.997
		Geography.Regions.Asia.North Asia            0.998
		Geography.Regions.Asia.South Asia            0.996
		Geography.Regions.Asia.Southeast Asia        0.998
		Geography.Regions.Asia.West Asia             0.998
		Geography.Regions.Europe.Eastern Europe      0.995
		Geography.Regions.Europe.Europe*             0.98
		Geography.Regions.Europe.Northern Europe     0.99
		Geography.Regions.Europe.Southern Europe     0.996
		Geography.Regions.Europe.Western Europe      0.994
		Geography.Regions.Oceania                    0.997
		History and Society.Business and economics   0.995
		History and Society.Education                0.995
		History and Society.History                  0.994
		History and Society.Military and warfare     0.994
		History and Society.Politics and government  0.988
		History and Society.Society                  0.994
		History and Society.Transportation           0.998
		STEM.Biology                                 0.991
		STEM.Chemistry                               0.999
		STEM.Computing                               0.999
		STEM.Earth and environment                   0.998
		STEM.Engineering                             0.998
		STEM.Libraries & Information                 1
		STEM.Mathematics                             1
		STEM.Medicine & Health                       0.998
		STEM.Physics                                 1
		STEM.STEM*                                   0.991
		STEM.Space                                   0.999
		STEM.Technology                              0.998
		-------------------------------------------  -----
	f1 (micro=0.712, macro=0.593):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.899
		Culture.Biography.Women                      0.514
		Culture.Food and drink                       0.556
		Culture.Internet culture                     0.569
		Culture.Linguistics                          0.699
		Culture.Literature                           0.598
		Culture.Media.Books                          0.668
		Culture.Media.Entertainment                  0.389
		Culture.Media.Films                          0.795
		Culture.Media.Media*                         0.685
		Culture.Media.Music                          0.745
		Culture.Media.Radio                          0.397
		Culture.Media.Software                       0.248
		Culture.Media.Television                     0.559
		Culture.Media.Video games                    0.798
		Culture.Performing arts                      0.416
		Culture.Philosophy and religion              0.472
		Culture.Sports                               0.904
		Culture.Visual arts.Architecture             0.66
		Culture.Visual arts.Comics and Anime         0.63
		Culture.Visual arts.Fashion                  0.399
		Culture.Visual arts.Visual arts*             0.663
		Geography.Geographical                       0.696
		Geography.Regions.Africa.Africa*             0.595
		Geography.Regions.Africa.Central Africa      0.515
		Geography.Regions.Africa.Eastern Africa      0.379
		Geography.Regions.Africa.Northern Africa     0.339
		Geography.Regions.Africa.Southern Africa     0.645
		Geography.Regions.Africa.Western Africa      0.522
		Geography.Regions.Americas.Central America   0.628
		Geography.Regions.Americas.North America     0.587
		Geography.Regions.Americas.South America     0.752
		Geography.Regions.Asia.Asia*                 0.771
		Geography.Regions.Asia.Central Asia          0.449
		Geography.Regions.Asia.East Asia             0.692
		Geography.Regions.Asia.North Asia            0.604
		Geography.Regions.Asia.South Asia            0.837
		Geography.Regions.Asia.Southeast Asia        0.76
		Geography.Regions.Asia.West Asia             0.718
		Geography.Regions.Europe.Eastern Europe      0.75
		Geography.Regions.Europe.Europe*             0.693
		Geography.Regions.Europe.Northern Europe     0.647
		Geography.Regions.Europe.Southern Europe     0.695
		Geography.Regions.Europe.Western Europe      0.664
		Geography.Regions.Oceania                    0.855
		History and Society.Business and economics   0.461
		History and Society.Education                0.474
		History and Society.History                  0.348
		History and Society.Military and warfare     0.611
		History and Society.Politics and government  0.595
		History and Society.Society                  0.246
		History and Society.Transportation           0.851
		STEM.Biology                                 0.817
		STEM.Chemistry                               0.426
		STEM.Computing                               0.371
		STEM.Earth and environment                   0.522
		STEM.Engineering                             0.592
		STEM.Libraries & Information                 0.415
		STEM.Mathematics                             0.336
		STEM.Medicine & Health                       0.601
		STEM.Physics                                 0.237
		STEM.STEM*                                   0.782
		STEM.Space                                   0.836
		STEM.Technology                              0.364
		-------------------------------------------  -----
	!f1 (micro=0.988, macro=0.995):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.985
		Culture.Biography.Women                      0.99
		Culture.Food and drink                       0.999
		Culture.Internet culture                     0.998
		Culture.Linguistics                          0.998
		Culture.Literature                           0.993
		Culture.Media.Books                          0.998
		Culture.Media.Entertainment                  0.997
		Culture.Media.Films                          0.998
		Culture.Media.Media*                         0.978
		Culture.Media.Music                          0.995
		Culture.Media.Radio                          0.999
		Culture.Media.Software                       0.997
		Culture.Media.Television                     0.996
		Culture.Media.Video games                    0.999
		Culture.Performing arts                      0.998
		Culture.Philosophy and religion              0.994
		Culture.Sports                               0.994
		Culture.Visual arts.Architecture             0.996
		Culture.Visual arts.Comics and Anime         0.999
		Culture.Visual arts.Fashion                  0.999
		Culture.Visual arts.Visual arts*             0.993
		Geography.Geographical                       0.993
		Geography.Regions.Africa.Africa*             0.995
		Geography.Regions.Africa.Central Africa      1
		Geography.Regions.Africa.Eastern Africa      0.999
		Geography.Regions.Africa.Northern Africa     0.998
		Geography.Regions.Africa.Southern Africa     0.999
		Geography.Regions.Africa.Western Africa      1
		Geography.Regions.Americas.Central America   0.999
		Geography.Regions.Americas.North America     0.968
		Geography.Regions.Americas.South America     0.998
		Geography.Regions.Asia.Asia*                 0.986
		Geography.Regions.Asia.Central Asia          0.999
		Geography.Regions.Asia.East Asia             0.996
		Geography.Regions.Asia.North Asia            0.997
		Geography.Regions.Asia.South Asia            0.998
		Geography.Regions.Asia.Southeast Asia        0.999
		Geography.Regions.Asia.West Asia             0.996
		Geography.Regions.Europe.Eastern Europe      0.995
		Geography.Regions.Europe.Europe*             0.969
		Geography.Regions.Europe.Northern Europe     0.989
		Geography.Regions.Europe.Southern Europe     0.996
		Geography.Regions.Europe.Western Europe      0.992
		Geography.Regions.Oceania                    0.998
		History and Society.Business and economics   0.994
		History and Society.Education                0.996
		History and Society.History                  0.991
		History and Society.Military and warfare     0.994
		History and Society.Politics and government  0.988
		History and Society.Society                  0.994
		History and Society.Transportation           0.998
		STEM.Biology                                 0.994
		STEM.Chemistry                               0.998
		STEM.Computing                               0.996
		STEM.Earth and environment                   0.998
		STEM.Engineering                             0.997
		STEM.Libraries & Information                 0.999
		STEM.Mathematics                             0.999
		STEM.Medicine & Health                       0.997
		STEM.Physics                                 0.998
		STEM.STEM*                                   0.983
		STEM.Space                                   0.999
		STEM.Technology                              0.994
		-------------------------------------------  -----
	accuracy (micro=0.977, macro=0.99):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.974
		Culture.Biography.Women                      0.98
		Culture.Food and drink                       0.997
		Culture.Internet culture                     0.995
		Culture.Linguistics                          0.996
		Culture.Literature                           0.986
		Culture.Media.Books                          0.997
		Culture.Media.Entertainment                  0.994
		Culture.Media.Films                          0.995
		Culture.Media.Media*                         0.958
		Culture.Media.Music                          0.989
		Culture.Media.Radio                          0.997
		Culture.Media.Software                       0.994
		Culture.Media.Television                     0.992
		Culture.Media.Video games                    0.999
		Culture.Performing arts                      0.996
		Culture.Philosophy and religion              0.989
		Culture.Sports                               0.988
		Culture.Visual arts.Architecture             0.993
		Culture.Visual arts.Comics and Anime         0.998
		Culture.Visual arts.Fashion                  0.998
		Culture.Visual arts.Visual arts*             0.987
		Geography.Geographical                       0.987
		Geography.Regions.Africa.Africa*             0.991
		Geography.Regions.Africa.Central Africa      0.999
		Geography.Regions.Africa.Eastern Africa      0.999
		Geography.Regions.Africa.Northern Africa     0.996
		Geography.Regions.Africa.Southern Africa     0.999
		Geography.Regions.Africa.Western Africa      0.999
		Geography.Regions.Americas.Central America   0.997
		Geography.Regions.Americas.North America     0.941
		Geography.Regions.Americas.South America     0.997
		Geography.Regions.Asia.Asia*                 0.974
		Geography.Regions.Asia.Central Asia          0.999
		Geography.Regions.Asia.East Asia             0.992
		Geography.Regions.Asia.North Asia            0.995
		Geography.Regions.Asia.South Asia            0.995
		Geography.Regions.Asia.Southeast Asia        0.997
		Geography.Regions.Asia.West Asia             0.993
		Geography.Regions.Europe.Eastern Europe      0.991
		Geography.Regions.Europe.Europe*             0.945
		Geography.Regions.Europe.Northern Europe     0.979
		Geography.Regions.Europe.Southern Europe     0.991
		Geography.Regions.Europe.Western Europe      0.985
		Geography.Regions.Oceania                    0.996
		History and Society.Business and economics   0.988
		History and Society.Education                0.992
		History and Society.History                  0.983
		History and Society.Military and warfare     0.988
		History and Society.Politics and government  0.977
		History and Society.Society                  0.988
		History and Society.Transportation           0.995
		STEM.Biology                                 0.988
		STEM.Chemistry                               0.997
		STEM.Computing                               0.993
		STEM.Earth and environment                   0.995
		STEM.Engineering                             0.994
		STEM.Libraries & Information                 0.999
		STEM.Mathematics                             0.999
		STEM.Medicine & Health                       0.994
		STEM.Physics                                 0.996
		STEM.STEM*                                   0.969
		STEM.Space                                   0.999
		STEM.Technology                              0.988
		-------------------------------------------  -----
	fpr (micro=0.016, macro=0.007):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.024
		Culture.Biography.Women                      0.016
		Culture.Food and drink                       0.002
		Culture.Internet culture                     0.004
		Culture.Linguistics                          0.001
		Culture.Literature                           0.01
		Culture.Media.Books                          0.002
		Culture.Media.Entertainment                  0.004
		Culture.Media.Films                          0.002
		Culture.Media.Media*                         0.034
		Culture.Media.Music                          0.005
		Culture.Media.Radio                          0.002
		Culture.Media.Software                       0.005
		Culture.Media.Television                     0.004
		Culture.Media.Video games                    0.001
		Culture.Performing arts                      0.003
		Culture.Philosophy and religion              0.006
		Culture.Sports                               0.007
		Culture.Visual arts.Architecture             0.003
		Culture.Visual arts.Comics and Anime         0.002
		Culture.Visual arts.Fashion                  0.002
		Culture.Visual arts.Visual arts*             0.008
		Geography.Geographical                       0.007
		Geography.Regions.Africa.Africa*             0.008
		Geography.Regions.Africa.Central Africa      0.001
		Geography.Regions.Africa.Eastern Africa      0.001
		Geography.Regions.Africa.Northern Africa     0.003
		Geography.Regions.Africa.Southern Africa     0.001
		Geography.Regions.Africa.Western Africa      0.001
		Geography.Regions.Americas.Central America   0.001
		Geography.Regions.Americas.North America     0.041
		Geography.Regions.Americas.South America     0.002
		Geography.Regions.Asia.Asia*                 0.018
		Geography.Regions.Asia.Central Asia          0.001
		Geography.Regions.Asia.East Asia             0.004
		Geography.Regions.Asia.North Asia            0.004
		Geography.Regions.Asia.South Asia            0.001
		Geography.Regions.Asia.Southeast Asia        0.001
		Geography.Regions.Asia.West Asia             0.005
		Geography.Regions.Europe.Eastern Europe      0.004
		Geography.Regions.Europe.Europe*             0.041
		Geography.Regions.Europe.Northern Europe     0.011
		Geography.Regions.Europe.Southern Europe     0.005
		Geography.Regions.Europe.Western Europe      0.01
		Geography.Regions.Oceania                    0.001
		History and Society.Business and economics   0.008
		History and Society.Education                0.003
		History and Society.History                  0.011
		History and Society.Military and warfare     0.007
		History and Society.Politics and government  0.011
		History and Society.Society                  0.006
		History and Society.Transportation           0.003
		STEM.Biology                                 0.004
		STEM.Chemistry                               0.003
		STEM.Computing                               0.006
		STEM.Earth and environment                   0.003
		STEM.Engineering                             0.004
		STEM.Libraries & Information                 0.001
		STEM.Mathematics                             0.001
		STEM.Medicine & Health                       0.004
		STEM.Physics                                 0.003
		STEM.STEM*                                   0.024
		STEM.Space                                   0.001
		STEM.Technology                              0.01
		-------------------------------------------  -----
	roc_auc (micro=0.971, macro=0.972):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.986
		Culture.Biography.Women                      0.978
		Culture.Food and drink                       0.975
		Culture.Internet culture                     0.983
		Culture.Linguistics                          0.97
		Culture.Literature                           0.973
		Culture.Media.Books                          0.98
		Culture.Media.Entertainment                  0.964
		Culture.Media.Films                          0.981
		Culture.Media.Media*                         0.971
		Culture.Media.Music                          0.979
		Culture.Media.Radio                          0.966
		Culture.Media.Software                       0.981
		Culture.Media.Television                     0.974
		Culture.Media.Video games                    0.992
		Culture.Performing arts                      0.967
		Culture.Philosophy and religion              0.942
		Culture.Sports                               0.981
		Culture.Visual arts.Architecture             0.976
		Culture.Visual arts.Comics and Anime         0.984
		Culture.Visual arts.Fashion                  0.98
		Culture.Visual arts.Visual arts*             0.968
		Geography.Geographical                       0.975
		Geography.Regions.Africa.Africa*             0.977
		Geography.Regions.Africa.Central Africa      0.979
		Geography.Regions.Africa.Eastern Africa      0.977
		Geography.Regions.Africa.Northern Africa     0.98
		Geography.Regions.Africa.Southern Africa     0.977
		Geography.Regions.Africa.Western Africa      0.979
		Geography.Regions.Americas.Central America   0.973
		Geography.Regions.Americas.North America     0.953
		Geography.Regions.Americas.South America     0.978
		Geography.Regions.Asia.Asia*                 0.971
		Geography.Regions.Asia.Central Asia          0.977
		Geography.Regions.Asia.East Asia             0.977
		Geography.Regions.Asia.North Asia            0.975
		Geography.Regions.Asia.South Asia            0.983
		Geography.Regions.Asia.Southeast Asia        0.977
		Geography.Regions.Asia.West Asia             0.981
		Geography.Regions.Europe.Eastern Europe      0.975
		Geography.Regions.Europe.Europe*             0.958
		Geography.Regions.Europe.Northern Europe     0.966
		Geography.Regions.Europe.Southern Europe     0.977
		Geography.Regions.Europe.Western Europe      0.975
		Geography.Regions.Oceania                    0.98
		History and Society.Business and economics   0.957
		History and Society.Education                0.956
		History and Society.History                  0.936
		History and Society.Military and warfare     0.967
		History and Society.Politics and government  0.951
		History and Society.Society                  0.893
		History and Society.Transportation           0.982
		STEM.Biology                                 0.974
		STEM.Chemistry                               0.984
		STEM.Computing                               0.984
		STEM.Earth and environment                   0.969
		STEM.Engineering                             0.976
		STEM.Libraries & Information                 0.972
		STEM.Mathematics                             0.978
		STEM.Medicine & Health                       0.973
		STEM.Physics                                 0.982
		STEM.STEM*                                   0.974
		STEM.Space                                   0.991
		STEM.Technology                              0.97
		-------------------------------------------  -----
	pr_auc (micro=0.753, macro=0.597):
		-------------------------------------------  -----
		Culture.Biography.Biography*                 0.951
		Culture.Biography.Women                      0.492
		Culture.Food and drink                       0.542
		Culture.Internet culture                     0.663
		Culture.Linguistics                          0.712
		Culture.Literature                           0.685
		Culture.Media.Books                          0.651
		Culture.Media.Entertainment                  0.369
		Culture.Media.Films                          0.821
		Culture.Media.Media*                         0.787
		Culture.Media.Music                          0.767
		Culture.Media.Radio                          0.32
		Culture.Media.Software                       0.222
		Culture.Media.Television                     0.593
		Culture.Media.Video games                    0.881
		Culture.Performing arts                      0.335
		Culture.Philosophy and religion              0.435
		Culture.Sports                               0.922
		Culture.Visual arts.Architecture             0.662
		Culture.Visual arts.Comics and Anime         0.651
		Culture.Visual arts.Fashion                  0.381
		Culture.Visual arts.Visual arts*             0.699
		Geography.Geographical                       0.746
		Geography.Regions.Africa.Africa*             0.607
		Geography.Regions.Africa.Central Africa      0.462
		Geography.Regions.Africa.Eastern Africa      0.313
		Geography.Regions.Africa.Northern Africa     0.271
		Geography.Regions.Africa.Southern Africa     0.514
		Geography.Regions.Africa.Western Africa      0.388
		Geography.Regions.Americas.Central America   0.594
		Geography.Regions.Americas.North America     0.609
		Geography.Regions.Americas.South America     0.728
		Geography.Regions.Asia.Asia*                 0.841
		Geography.Regions.Asia.Central Asia          0.354
		Geography.Regions.Asia.East Asia             0.715
		Geography.Regions.Asia.North Asia            0.654
		Geography.Regions.Asia.South Asia            0.878
		Geography.Regions.Asia.Southeast Asia        0.778
		Geography.Regions.Asia.West Asia             0.743
		Geography.Regions.Europe.Eastern Europe      0.796
		Geography.Regions.Europe.Europe*             0.774
		Geography.Regions.Europe.Northern Europe     0.696
		Geography.Regions.Europe.Southern Europe     0.742
		Geography.Regions.Europe.Western Europe      0.686
		Geography.Regions.Oceania                    0.872
		History and Society.Business and economics   0.416
		History and Society.Education                0.471
		History and Society.History                  0.273
		History and Society.Military and warfare     0.632
		History and Society.Politics and government  0.624
		History and Society.Society                  0.162
		History and Society.Transportation           0.897
		STEM.Biology                                 0.86
		STEM.Chemistry                               0.418
		STEM.Computing                               0.38
		STEM.Earth and environment                   0.48
		STEM.Engineering                             0.657
		STEM.Libraries & Information                 0.323
		STEM.Mathematics                             0.375
		STEM.Medicine & Health                       0.659
		STEM.Physics                                 0.18
		STEM.STEM*                                   0.883
		STEM.Space                                   0.877
		STEM.Technology                              0.351
		-------------------------------------------  -----
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely labels predicted by the estimator', 'type': 'array', 'items': {'type': 'string'}}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'Culture.Biography.Biography*': {'type': 'number'}, 'Culture.Biography.Women': {'type': 'number'}, 'Culture.Food and drink': {'type': 'number'}, 'Culture.Internet culture': {'type': 'number'}, 'Culture.Linguistics': {'type': 'number'}, 'Culture.Literature': {'type': 'number'}, 'Culture.Media.Books': {'type': 'number'}, 'Culture.Media.Entertainment': {'type': 'number'}, 'Culture.Media.Films': {'type': 'number'}, 'Culture.Media.Media*': {'type': 'number'}, 'Culture.Media.Music': {'type': 'number'}, 'Culture.Media.Radio': {'type': 'number'}, 'Culture.Media.Software': {'type': 'number'}, 'Culture.Media.Television': {'type': 'number'}, 'Culture.Media.Video games': {'type': 'number'}, 'Culture.Performing arts': {'type': 'number'}, 'Culture.Philosophy and religion': {'type': 'number'}, 'Culture.Sports': {'type': 'number'}, 'Culture.Visual arts.Architecture': {'type': 'number'}, 'Culture.Visual arts.Comics and Anime': {'type': 'number'}, 'Culture.Visual arts.Fashion': {'type': 'number'}, 'Culture.Visual arts.Visual arts*': {'type': 'number'}, 'Geography.Geographical': {'type': 'number'}, 'Geography.Regions.Africa.Africa*': {'type': 'number'}, 'Geography.Regions.Africa.Central Africa': {'type': 'number'}, 'Geography.Regions.Africa.Eastern Africa': {'type': 'number'}, 'Geography.Regions.Africa.Northern Africa': {'type': 'number'}, 'Geography.Regions.Africa.Southern Africa': {'type': 'number'}, 'Geography.Regions.Africa.Western Africa': {'type': 'number'}, 'Geography.Regions.Americas.Central America': {'type': 'number'}, 'Geography.Regions.Americas.North America': {'type': 'number'}, 'Geography.Regions.Americas.South America': {'type': 'number'}, 'Geography.Regions.Asia.Asia*': {'type': 'number'}, 'Geography.Regions.Asia.Central Asia': {'type': 'number'}, 'Geography.Regions.Asia.East Asia': {'type': 'number'}, 'Geography.Regions.Asia.North Asia': {'type': 'number'}, 'Geography.Regions.Asia.South Asia': {'type': 'number'}, 'Geography.Regions.Asia.Southeast Asia': {'type': 'number'}, 'Geography.Regions.Asia.West Asia': {'type': 'number'}, 'Geography.Regions.Europe.Eastern Europe': {'type': 'number'}, 'Geography.Regions.Europe.Europe*': {'type': 'number'}, 'Geography.Regions.Europe.Northern Europe': {'type': 'number'}, 'Geography.Regions.Europe.Southern Europe': {'type': 'number'}, 'Geography.Regions.Europe.Western Europe': {'type': 'number'}, 'Geography.Regions.Oceania': {'type': 'number'}, 'History and Society.Business and economics': {'type': 'number'}, 'History and Society.Education': {'type': 'number'}, 'History and Society.History': {'type': 'number'}, 'History and Society.Military and warfare': {'type': 'number'}, 'History and Society.Politics and government': {'type': 'number'}, 'History and Society.Society': {'type': 'number'}, 'History and Society.Transportation': {'type': 'number'}, 'STEM.Biology': {'type': 'number'}, 'STEM.Chemistry': {'type': 'number'}, 'STEM.Computing': {'type': 'number'}, 'STEM.Earth and environment': {'type': 'number'}, 'STEM.Engineering': {'type': 'number'}, 'STEM.Libraries & Information': {'type': 'number'}, 'STEM.Mathematics': {'type': 'number'}, 'STEM.Medicine & Health': {'type': 'number'}, 'STEM.Physics': {'type': 'number'}, 'STEM.STEM*': {'type': 'number'}, 'STEM.Space': {'type': 'number'}, 'STEM.Technology': {'type': 'number'}}}}}

