Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.5, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18208):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     18122  -->    18103        19
		False       86  -->       85         1
	rates:
		              True    False
		----------  ------  -------
		sample       0.995    0.005
		population   0.994    0.006
	match_rate (micro=0.993, macro=0.5):
		  True    False
		------  -------
		 0.999    0.001
	filter_rate (micro=0.007, macro=0.5):
		  True    False
		------  -------
		 0.001    0.999
	recall (micro=0.993, macro=0.505):
		  True    False
		------  -------
		 0.999    0.012
	!recall (micro=0.018, macro=0.505):
		  True    False
		------  -------
		 0.012    0.999
	precision (micro=0.988, macro=0.529):
		  True    False
		------  -------
		 0.994    0.064
	!precision (micro=0.07, macro=0.529):
		  True    False
		------  -------
		 0.064    0.994
	f1 (micro=0.99, macro=0.508):
		  True    False
		------  -------
		 0.996     0.02
	!f1 (micro=0.026, macro=0.508):
		  True    False
		------  -------
		  0.02    0.996
	accuracy (micro=0.993, macro=0.993):
		  True    False
		------  -------
		 0.993    0.993
	fpr (micro=0.982, macro=0.495):
		  True    False
		------  -------
		 0.988    0.001
	roc_auc (micro=0.977, macro=0.803):
		  True    False
		------  -------
		 0.979    0.627
	pr_auc (micro=0.99, macro=0.521):
		  True    False
		------  -------
		 0.995    0.046
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

