Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18247):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1006  -->      834       172
		False    17241  -->     1267     15974
	rates:
		              True    False
		----------  ------  -------
		sample       0.055    0.945
		population   0.051    0.949
	match_rate (micro=0.849, macro=0.5):
		  True    False
		------  -------
		 0.112    0.888
	filter_rate (micro=0.151, macro=0.5):
		  True    False
		------  -------
		 0.888    0.112
	recall (micro=0.922, macro=0.878):
		  True    False
		------  -------
		 0.829    0.927
	!recall (micro=0.834, macro=0.878):
		  True    False
		------  -------
		 0.927    0.829
	precision (micro=0.959, macro=0.683):
		  True    False
		------  -------
		 0.376     0.99
	!precision (micro=0.407, macro=0.683):
		  True    False
		------  -------
		  0.99    0.376
	f1 (micro=0.935, macro=0.737):
		  True    False
		------  -------
		 0.517    0.957
	!f1 (micro=0.54, macro=0.737):
		  True    False
		------  -------
		 0.957    0.517
	accuracy (micro=0.922, macro=0.922):
		  True    False
		------  -------
		 0.922    0.922
	fpr (micro=0.166, macro=0.122):
		  True    False
		------  -------
		 0.073    0.171
	roc_auc (micro=0.953, macro=0.953):
		  True    False
		------  -------
		 0.953    0.953
	pr_auc (micro=0.979, macro=0.816):
		  True    False
		------  -------
		 0.636    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

