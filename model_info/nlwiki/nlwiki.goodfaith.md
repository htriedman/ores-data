Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18247):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17625  -->    16948       677
		False      622  -->      112       510
	rates:
		              True    False
		----------  ------  -------
		sample       0.966    0.034
		population   0.965    0.035
	match_rate (micro=0.903, macro=0.5):
		  True    False
		------  -------
		 0.934    0.066
	filter_rate (micro=0.097, macro=0.5):
		  True    False
		------  -------
		 0.066    0.934
	recall (micro=0.957, macro=0.891):
		  True    False
		------  -------
		 0.962     0.82
	!recall (micro=0.825, macro=0.891):
		  True    False
		------  -------
		  0.82    0.962
	precision (micro=0.974, macro=0.716):
		  True    False
		------  -------
		 0.993    0.439
	!precision (micro=0.459, macro=0.716):
		  True    False
		------  -------
		 0.439    0.993
	f1 (micro=0.963, macro=0.775):
		  True    False
		------  -------
		 0.977    0.572
	!f1 (micro=0.586, macro=0.775):
		  True    False
		------  -------
		 0.572    0.977
	accuracy (micro=0.957, macro=0.957):
		  True    False
		------  -------
		 0.957    0.957
	fpr (micro=0.175, macro=0.109):
		  True    False
		------  -------
		  0.18    0.038
	roc_auc (micro=0.972, macro=0.972):
		  True    False
		------  -------
		 0.972    0.971
	pr_auc (micro=0.988, macro=0.854):
		  True    False
		------  -------
		 0.999    0.709
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

