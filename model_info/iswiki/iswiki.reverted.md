Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19800):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1471  -->      936       535
		False    18329  -->      720     17609
	rates:
		              True    False
		----------  ------  -------
		sample       0.074    0.926
		population   0.081    0.919
	match_rate (micro=0.845, macro=0.5):
		  True    False
		------  -------
		 0.088    0.912
	filter_rate (micro=0.155, macro=0.5):
		  True    False
		------  -------
		 0.912    0.088
	recall (micro=0.934, macro=0.799):
		  True    False
		------  -------
		 0.636    0.961
	!recall (micro=0.663, macro=0.799):
		  True    False
		------  -------
		 0.961    0.636
	precision (micro=0.937, macro=0.778):
		  True    False
		------  -------
		 0.589    0.968
	!precision (micro=0.619, macro=0.778):
		  True    False
		------  -------
		 0.968    0.589
	f1 (micro=0.936, macro=0.788):
		  True    False
		------  -------
		 0.612    0.964
	!f1 (micro=0.64, macro=0.788):
		  True    False
		------  -------
		 0.964    0.612
	accuracy (micro=0.934, macro=0.934):
		  True    False
		------  -------
		 0.934    0.934
	fpr (micro=0.337, macro=0.201):
		  True    False
		------  -------
		 0.039    0.364
	roc_auc (micro=0.939, macro=0.937):
		  True    False
		------  -------
		 0.936    0.939
	pr_auc (micro=0.963, macro=0.812):
		  True    False
		------  -------
		 0.631    0.993
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

