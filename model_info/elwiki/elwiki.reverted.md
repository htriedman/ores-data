Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19685):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       946  -->      598       348
		False    18739  -->     1372     17367
	rates:
		              True    False
		----------  ------  -------
		sample       0.048    0.952
		population   0.052    0.948
	match_rate (micro=0.857, macro=0.5):
		  True    False
		------  -------
		 0.102    0.898
	filter_rate (micro=0.143, macro=0.5):
		  True    False
		------  -------
		 0.898    0.102
	recall (micro=0.912, macro=0.779):
		  True    False
		------  -------
		 0.632    0.927
	!recall (micro=0.647, macro=0.779):
		  True    False
		------  -------
		 0.927    0.632
	precision (micro=0.945, macro=0.649):
		  True    False
		------  -------
		  0.32    0.979
	!precision (micro=0.354, macro=0.649):
		  True    False
		------  -------
		 0.979     0.32
	f1 (micro=0.925, macro=0.689):
		  True    False
		------  -------
		 0.425    0.952
	!f1 (micro=0.452, macro=0.689):
		  True    False
		------  -------
		 0.952    0.425
	accuracy (micro=0.912, macro=0.912):
		  True    False
		------  -------
		 0.912    0.912
	fpr (micro=0.353, macro=0.221):
		  True    False
		------  -------
		 0.073    0.368
	roc_auc (micro=0.911, macro=0.911):
		  True    False
		------  -------
		  0.91    0.911
	pr_auc (micro=0.965, macro=0.716):
		  True    False
		------  -------
		 0.437    0.994
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

