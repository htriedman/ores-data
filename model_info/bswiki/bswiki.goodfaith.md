Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.5, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=34887):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     34005  -->    33737       268
		False      882  -->      358       524
	rates:
		              True    False
		----------  ------  -------
		sample       0.975    0.025
		population   0.977    0.023
	match_rate (micro=0.957, macro=0.5):
		  True    False
		------  -------
		 0.979    0.021
	filter_rate (micro=0.043, macro=0.5):
		  True    False
		------  -------
		 0.021    0.979
	recall (micro=0.983, macro=0.793):
		  True    False
		------  -------
		 0.992    0.594
	!recall (micro=0.603, macro=0.793):
		  True    False
		------  -------
		 0.594    0.992
	precision (micro=0.983, macro=0.813):
		  True    False
		------  -------
		 0.991    0.634
	!precision (micro=0.642, macro=0.813):
		  True    False
		------  -------
		 0.634    0.991
	f1 (micro=0.983, macro=0.803):
		  True    False
		------  -------
		 0.991    0.614
	!f1 (micro=0.622, macro=0.803):
		  True    False
		------  -------
		 0.614    0.991
	accuracy (micro=0.983, macro=0.983):
		  True    False
		------  -------
		 0.983    0.983
	fpr (micro=0.397, macro=0.207):
		  True    False
		------  -------
		 0.406    0.008
	roc_auc (micro=0.988, macro=0.946):
		  True    False
		------  -------
		  0.99    0.902
	pr_auc (micro=0.987, macro=0.797):
		  True    False
		------  -------
		 0.996    0.599
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

