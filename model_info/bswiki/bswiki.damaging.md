Model Information:
	 - type: RandomForest
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': None, 'criterion': 'entropy', 'max_depth': None, 'max_features': 'log2', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 7, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 320, 'n_jobs': None, 'oob_score': False, 'random_state': None, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=34887):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1099  -->      470       629
		False    33788  -->      139     33649
	rates:
		              True    False
		----------  ------  -------
		sample       0.032    0.968
		population   0.028    0.972
	match_rate (micro=0.957, macro=0.5):
		  True    False
		------  -------
		 0.016    0.984
	filter_rate (micro=0.043, macro=0.5):
		  True    False
		------  -------
		 0.984    0.016
	recall (micro=0.98, macro=0.712):
		  True    False
		------  -------
		 0.428    0.996
	!recall (micro=0.444, macro=0.712):
		  True    False
		------  -------
		 0.996    0.428
	precision (micro=0.977, macro=0.867):
		  True    False
		------  -------
		  0.75    0.984
	!precision (micro=0.757, macro=0.867):
		  True    False
		------  -------
		 0.984     0.75
	f1 (micro=0.977, macro=0.767):
		  True    False
		------  -------
		 0.545     0.99
	!f1 (micro=0.557, macro=0.767):
		  True    False
		------  -------
		  0.99    0.545
	accuracy (micro=0.98, macro=0.98):
		  True    False
		------  -------
		  0.98     0.98
	fpr (micro=0.556, macro=0.288):
		  True    False
		------  -------
		 0.004    0.572
	roc_auc (micro=0.979, macro=0.979):
		  True    False
		------  -------
		 0.979    0.979
	pr_auc (micro=0.99, macro=0.832):
		  True    False
		------  -------
		 0.664    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

