Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19398):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     18773  -->    18372       401
		False      625  -->      416       209
	rates:
		              True    False
		----------  ------  -------
		sample       0.968    0.032
		population   0.981    0.019
	match_rate (micro=0.954, macro=0.5):
		  True    False
		------  -------
		 0.973    0.027
	filter_rate (micro=0.046, macro=0.5):
		  True    False
		------  -------
		 0.027    0.973
	recall (micro=0.966, macro=0.657):
		  True    False
		------  -------
		 0.979    0.334
	!recall (micro=0.347, macro=0.657):
		  True    False
		------  -------
		 0.334    0.979
	precision (micro=0.972, macro=0.611):
		  True    False
		------  -------
		 0.987    0.235
	!precision (micro=0.249, macro=0.611):
		  True    False
		------  -------
		 0.235    0.987
	f1 (micro=0.969, macro=0.629):
		  True    False
		------  -------
		 0.983    0.276
	!f1 (micro=0.289, macro=0.629):
		  True    False
		------  -------
		 0.276    0.983
	accuracy (micro=0.966, macro=0.966):
		  True    False
		------  -------
		 0.966    0.966
	fpr (micro=0.653, macro=0.343):
		  True    False
		------  -------
		 0.666    0.021
	roc_auc (micro=0.899, macro=0.899):
		  True    False
		------  -------
		 0.899    0.899
	pr_auc (micro=0.983, macro=0.625):
		  True    False
		------  -------
		 0.998    0.252
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

