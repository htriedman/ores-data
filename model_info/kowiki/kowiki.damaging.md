Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19398):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       922  -->      446       476
		False    18476  -->     1327     17149
	rates:
		              True    False
		----------  ------  -------
		sample       0.048    0.952
		population   0.039    0.961
	match_rate (micro=0.88, macro=0.5):
		  True    False
		------  -------
		 0.088    0.912
	filter_rate (micro=0.12, macro=0.5):
		  True    False
		------  -------
		 0.912    0.088
	recall (micro=0.911, macro=0.706):
		  True    False
		------  -------
		 0.484    0.928
	!recall (micro=0.501, macro=0.706):
		  True    False
		------  -------
		 0.928    0.484
	precision (micro=0.949, macro=0.596):
		  True    False
		------  -------
		 0.213    0.978
	!precision (micro=0.243, macro=0.596):
		  True    False
		------  -------
		 0.978    0.213
	f1 (micro=0.927, macro=0.624):
		  True    False
		------  -------
		 0.296    0.952
	!f1 (micro=0.321, macro=0.624):
		  True    False
		------  -------
		 0.952    0.296
	accuracy (micro=0.911, macro=0.911):
		  True    False
		------  -------
		 0.911    0.911
	fpr (micro=0.499, macro=0.294):
		  True    False
		------  -------
		 0.072    0.516
	roc_auc (micro=0.887, macro=0.887):
		  True    False
		------  -------
		 0.888    0.887
	pr_auc (micro=0.968, macro=0.654):
		  True    False
		------  -------
		 0.314    0.995
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

