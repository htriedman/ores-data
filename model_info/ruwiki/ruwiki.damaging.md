Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18066):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      1049  -->      883       166
		False    17017  -->     2363     14654
	rates:
		              True    False
		----------  ------  -------
		sample       0.058    0.942
		population   0.053    0.947
	match_rate (micro=0.789, macro=0.5):
		  True    False
		------  -------
		 0.176    0.824
	filter_rate (micro=0.211, macro=0.5):
		  True    False
		------  -------
		 0.824    0.176
	recall (micro=0.86, macro=0.851):
		  True    False
		------  -------
		 0.842    0.861
	!recall (micro=0.843, macro=0.851):
		  True    False
		------  -------
		 0.861    0.842
	precision (micro=0.95, macro=0.622):
		  True    False
		------  -------
		 0.255     0.99
	!precision (micro=0.294, macro=0.622):
		  True    False
		------  -------
		  0.99    0.255
	f1 (micro=0.893, macro=0.656):
		  True    False
		------  -------
		 0.392    0.921
	!f1 (micro=0.42, macro=0.656):
		  True    False
		------  -------
		 0.921    0.392
	accuracy (micro=0.86, macro=0.86):
		  True    False
		------  -------
		  0.86     0.86
	fpr (micro=0.157, macro=0.149):
		  True    False
		------  -------
		 0.139    0.158
	roc_auc (micro=0.925, macro=0.926):
		  True    False
		------  -------
		 0.928    0.924
	pr_auc (micro=0.964, macro=0.704):
		  True    False
		------  -------
		 0.412    0.996
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

