Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18066):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17547  -->    16669       878
		False      519  -->      237       282
	rates:
		              True    False
		----------  ------  -------
		sample       0.971    0.029
		population   0.971    0.029
	match_rate (micro=0.911, macro=0.5):
		  True    False
		------  -------
		 0.936    0.064
	filter_rate (micro=0.089, macro=0.5):
		  True    False
		------  -------
		 0.064    0.936
	recall (micro=0.938, macro=0.747):
		  True    False
		------  -------
		  0.95    0.543
	!recall (micro=0.555, macro=0.747):
		  True    False
		------  -------
		 0.543     0.95
	precision (micro=0.965, macro=0.614):
		  True    False
		------  -------
		 0.986    0.242
	!precision (micro=0.264, macro=0.614):
		  True    False
		------  -------
		 0.242    0.986
	f1 (micro=0.95, macro=0.651):
		  True    False
		------  -------
		 0.968    0.335
	!f1 (micro=0.353, macro=0.651):
		  True    False
		------  -------
		 0.335    0.968
	accuracy (micro=0.938, macro=0.938):
		  True    False
		------  -------
		 0.938    0.938
	fpr (micro=0.445, macro=0.253):
		  True    False
		------  -------
		 0.457     0.05
	roc_auc (micro=0.931, macro=0.931):
		  True    False
		------  -------
		 0.931    0.931
	pr_auc (micro=0.978, macro=0.65):
		  True    False
		------  -------
		 0.998    0.303
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

