Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19131):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       296  -->       77       219
		False    18835  -->      207     18628
	rates:
		              True    False
		----------  ------  -------
		sample       0.015    0.985
		population   0.016    0.984
	match_rate (micro=0.97, macro=0.5):
		  True    False
		------  -------
		 0.015    0.985
	filter_rate (micro=0.03, macro=0.5):
		  True    False
		------  -------
		 0.985    0.015
	recall (micro=0.977, macro=0.625):
		  True    False
		------  -------
		  0.26    0.989
	!recall (micro=0.272, macro=0.625):
		  True    False
		------  -------
		 0.989     0.26
	precision (micro=0.977, macro=0.632):
		  True    False
		------  -------
		 0.277    0.988
	!precision (micro=0.288, macro=0.632):
		  True    False
		------  -------
		 0.988    0.277
	f1 (micro=0.977, macro=0.628):
		  True    False
		------  -------
		 0.268    0.989
	!f1 (micro=0.28, macro=0.628):
		  True    False
		------  -------
		 0.989    0.268
	accuracy (micro=0.977, macro=0.977):
		  True    False
		------  -------
		 0.977    0.977
	fpr (micro=0.728, macro=0.375):
		  True    False
		------  -------
		 0.011     0.74
	roc_auc (micro=0.886, macro=0.885):
		  True    False
		------  -------
		 0.885    0.886
	pr_auc (micro=0.985, macro=0.601):
		  True    False
		------  -------
		 0.204    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

