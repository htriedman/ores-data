Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=26606):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       600  -->      482       118
		False    26006  -->      704     25302
	rates:
		              True    False
		----------  ------  -------
		sample       0.023    0.977
		population   0.025    0.975
	match_rate (micro=0.931, macro=0.5):
		  True    False
		------  -------
		 0.047    0.953
	filter_rate (micro=0.069, macro=0.5):
		  True    False
		------  -------
		 0.953    0.047
	recall (micro=0.969, macro=0.888):
		  True    False
		------  -------
		 0.803    0.973
	!recall (micro=0.808, macro=0.888):
		  True    False
		------  -------
		 0.973    0.803
	precision (micro=0.981, macro=0.715):
		  True    False
		------  -------
		 0.434    0.995
	!precision (micro=0.448, macro=0.715):
		  True    False
		------  -------
		 0.995    0.434
	f1 (micro=0.973, macro=0.774):
		  True    False
		------  -------
		 0.564    0.984
	!f1 (micro=0.574, macro=0.774):
		  True    False
		------  -------
		 0.984    0.564
	accuracy (micro=0.969, macro=0.969):
		  True    False
		------  -------
		 0.969    0.969
	fpr (micro=0.192, macro=0.112):
		  True    False
		------  -------
		 0.027    0.197
	roc_auc (micro=0.971, macro=0.972):
		  True    False
		------  -------
		 0.973    0.971
	pr_auc (micro=0.991, macro=0.835):
		  True    False
		------  -------
		 0.671    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

