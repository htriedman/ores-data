Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=26606):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     26201  -->    25975       226
		False      405  -->      136       269
	rates:
		              True    False
		----------  ------  -------
		sample       0.985    0.015
		population   0.982    0.018
	match_rate (micro=0.963, macro=0.5):
		  True    False
		------  -------
		  0.98     0.02
	filter_rate (micro=0.037, macro=0.5):
		  True    False
		------  -------
		  0.02     0.98
	recall (micro=0.986, macro=0.828):
		  True    False
		------  -------
		 0.991    0.664
	!recall (micro=0.67, macro=0.828):
		  True    False
		------  -------
		 0.664    0.991
	precision (micro=0.987, macro=0.788):
		  True    False
		------  -------
		 0.994    0.581
	!precision (micro=0.589, macro=0.788):
		  True    False
		------  -------
		 0.581    0.994
	f1 (micro=0.986, macro=0.806):
		  True    False
		------  -------
		 0.993     0.62
	!f1 (micro=0.627, macro=0.806):
		  True    False
		------  -------
		  0.62    0.993
	accuracy (micro=0.986, macro=0.986):
		  True    False
		------  -------
		 0.986    0.986
	fpr (micro=0.33, macro=0.172):
		  True    False
		------  -------
		 0.336    0.009
	roc_auc (micro=0.977, macro=0.976):
		  True    False
		------  -------
		 0.977    0.974
	pr_auc (micro=0.994, macro=0.845):
		  True    False
		------  -------
		 0.999    0.691
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

