Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=117250):
		label         n         ~True    ~False
		-------  ------  ---  -------  --------
		True        415  -->      180       235
		False    116835  -->      248    116587
	rates:
		              True    False
		----------  ------  -------
		sample       0.004    0.996
		population   0.006    0.994
	match_rate (micro=0.99, macro=0.5):
		  True    False
		------  -------
		 0.005    0.995
	filter_rate (micro=0.01, macro=0.5):
		  True    False
		------  -------
		 0.995    0.005
	recall (micro=0.995, macro=0.716):
		  True    False
		------  -------
		 0.434    0.998
	!recall (micro=0.437, macro=0.716):
		  True    False
		------  -------
		 0.998    0.434
	precision (micro=0.994, macro=0.767):
		  True    False
		------  -------
		 0.536    0.997
	!precision (micro=0.539, macro=0.767):
		  True    False
		------  -------
		 0.997    0.536
	f1 (micro=0.994, macro=0.738):
		  True    False
		------  -------
		  0.48    0.997
	!f1 (micro=0.483, macro=0.738):
		  True    False
		------  -------
		 0.997     0.48
	accuracy (micro=0.995, macro=0.995):
		  True    False
		------  -------
		 0.995    0.995
	fpr (micro=0.563, macro=0.284):
		  True    False
		------  -------
		 0.002    0.566
	roc_auc (micro=0.989, macro=0.987):
		  True    False
		------  -------
		 0.984    0.989
	pr_auc (micro=0.997, macro=0.737):
		  True    False
		------  -------
		 0.474        1
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

