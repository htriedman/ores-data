Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=117250):
		label         n         ~True    ~False
		-------  ------  ---  -------  --------
		True     117104  -->   117008        96
		False       146  -->       71        75
	rates:
		              True    False
		----------  ------  -------
		sample       0.999    0.001
		population   0.996    0.004
	match_rate (micro=0.993, macro=0.5):
		  True    False
		------  -------
		 0.997    0.003
	filter_rate (micro=0.007, macro=0.5):
		  True    False
		------  -------
		 0.003    0.997
	recall (micro=0.997, macro=0.756):
		  True    False
		------  -------
		 0.999    0.514
	!recall (micro=0.516, macro=0.756):
		  True    False
		------  -------
		 0.514    0.999
	precision (micro=0.997, macro=0.852):
		  True    False
		------  -------
		 0.998    0.706
	!precision (micro=0.707, macro=0.852):
		  True    False
		------  -------
		 0.706    0.998
	f1 (micro=0.997, macro=0.797):
		  True    False
		------  -------
		 0.999    0.595
	!f1 (micro=0.596, macro=0.797):
		  True    False
		------  -------
		 0.595    0.999
	accuracy (micro=0.997, macro=0.997):
		  True    False
		------  -------
		 0.997    0.997
	fpr (micro=0.484, macro=0.244):
		  True    False
		------  -------
		 0.486    0.001
	roc_auc (micro=0.994, macro=0.99):
		  True    False
		------  -------
		 0.994    0.987
	pr_auc (micro=0.998, macro=0.792):
		  True    False
		------  -------
		     1    0.585
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

