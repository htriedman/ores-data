Model Information:
	 - type: GradientBoosting
	 - version: 0.3.0
	 - params: {'scale': True, 'center': True, 'labels': ['validated', 'not_proofread', 'proofread', 'without_text'], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=20000):
		label               n         ~validated    ~not_proofread    ~proofread    ~without_text
		---------------  ----  ---  ------------  ----------------  ------------  ---------------
		'validated'      5000  -->          3518               541           926               15
		'not_proofread'  5000  -->           833              3463           630               74
		'proofread'      5000  -->          1680               594          2708               18
		'without_text'   5000  -->            50               105            45             4800
	rates:
		              'validated'    'not_proofread'    'proofread'    'without_text'
		----------  -------------  -----------------  -------------  ----------------
		sample              0.25               0.25           0.25              0.25
		population          0.173              0.499          0.296             0.032
	match_rate (micro=0.31, macro=0.231):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.263            0.387        0.236           0.037
	filter_rate (micro=0.69, macro=0.769):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.737            0.613        0.764           0.963
	recall (micro=0.658, macro=0.724):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.704            0.693        0.542            0.96
	!recall (micro=0.897, macro=0.908):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.829            0.917        0.893           0.993
	precision (micro=0.753, macro=0.713):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.462            0.893        0.681           0.815
	!precision (micro=0.81, macro=0.875):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.931             0.75        0.822           0.999
	f1 (micro=0.693, macro=0.706):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.558             0.78        0.603           0.882
	!f1 (micro=0.849, macro=0.889):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.877            0.825        0.856           0.996
	accuracy (micro=0.807, macro=0.848):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.807            0.805        0.789           0.992
	fpr (micro=0.103, macro=0.092):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.171            0.083        0.107           0.007
	roc_auc (micro=0.892, macro=0.909):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.874            0.911         0.86           0.989
	pr_auc (micro=0.803, macro=0.789):
		  validated    not_proofread    proofread    without_text
		-----------  ---------------  -----------  --------------
		      0.564            0.914        0.741           0.936
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'validated': {'type': 'number'}, 'not_proofread': {'type': 'number'}, 'proofread': {'type': 'number'}, 'without_text': {'type': 'number'}}}}}

