Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=93200):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       613  -->      460       153
		False    92587  -->      732     91855
	rates:
		              True    False
		----------  ------  -------
		sample       0.007    0.993
		population   0.019    0.981
	match_rate (micro=0.961, macro=0.5):
		  True    False
		------  -------
		 0.022    0.978
	filter_rate (micro=0.039, macro=0.5):
		  True    False
		------  -------
		 0.978    0.022
	recall (micro=0.988, macro=0.871):
		  True    False
		------  -------
		  0.75    0.992
	!recall (micro=0.755, macro=0.871):
		  True    False
		------  -------
		 0.992     0.75
	precision (micro=0.989, macro=0.819):
		  True    False
		------  -------
		 0.642    0.995
	!precision (micro=0.648, macro=0.819):
		  True    False
		------  -------
		 0.995    0.642
	f1 (micro=0.988, macro=0.843):
		  True    False
		------  -------
		 0.692    0.994
	!f1 (micro=0.697, macro=0.843):
		  True    False
		------  -------
		 0.994    0.692
	accuracy (micro=0.988, macro=0.988):
		  True    False
		------  -------
		 0.988    0.988
	fpr (micro=0.245, macro=0.129):
		  True    False
		------  -------
		 0.008     0.25
	roc_auc (micro=0.967, macro=0.966):
		  True    False
		------  -------
		 0.965    0.968
	pr_auc (micro=0.995, macro=0.876):
		  True    False
		------  -------
		 0.754    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

