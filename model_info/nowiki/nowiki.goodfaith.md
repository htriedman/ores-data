Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=93200):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     92786  -->    92300       486
		False      414  -->       94       320
	rates:
		              True    False
		----------  ------  -------
		sample       0.996    0.004
		population   0.987    0.013
	match_rate (micro=0.971, macro=0.5):
		  True    False
		------  -------
		 0.984    0.016
	filter_rate (micro=0.029, macro=0.5):
		  True    False
		------  -------
		 0.016    0.984
	recall (micro=0.992, macro=0.884):
		  True    False
		------  -------
		 0.995    0.773
	!recall (micro=0.776, macro=0.884):
		  True    False
		------  -------
		 0.773    0.995
	precision (micro=0.992, macro=0.832):
		  True    False
		------  -------
		 0.997    0.667
	!precision (micro=0.672, macro=0.832):
		  True    False
		------  -------
		 0.667    0.997
	f1 (micro=0.992, macro=0.856):
		  True    False
		------  -------
		 0.996    0.716
	!f1 (micro=0.72, macro=0.856):
		  True    False
		------  -------
		 0.716    0.996
	accuracy (micro=0.992, macro=0.992):
		  True    False
		------  -------
		 0.992    0.992
	fpr (micro=0.224, macro=0.116):
		  True    False
		------  -------
		 0.227    0.005
	roc_auc (micro=0.986, macro=0.985):
		  True    False
		------  -------
		 0.986    0.984
	pr_auc (micro=0.997, macro=0.901):
		  True    False
		------  -------
		     1    0.803
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

