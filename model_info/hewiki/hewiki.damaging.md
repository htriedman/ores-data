Model Information:
	 - type: RandomForest
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': None, 'criterion': 'entropy', 'max_depth': None, 'max_features': 'log2', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 320, 'n_jobs': None, 'oob_score': False, 'random_state': None, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17634):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       580  -->       62       518
		False    17054  -->       22     17032
	rates:
		              True    False
		----------  ------  -------
		sample       0.033    0.967
		population   0.046    0.954
	match_rate (micro=0.948, macro=0.5):
		  True    False
		------  -------
		 0.006    0.994
	filter_rate (micro=0.052, macro=0.5):
		  True    False
		------  -------
		 0.994    0.006
	recall (micro=0.957, macro=0.553):
		  True    False
		------  -------
		 0.107    0.999
	!recall (micro=0.148, macro=0.553):
		  True    False
		------  -------
		 0.999    0.107
	precision (micro=0.951, macro=0.88):
		  True    False
		------  -------
		 0.801    0.958
	!precision (micro=0.808, macro=0.88):
		  True    False
		------  -------
		 0.958    0.801
	f1 (micro=0.942, macro=0.583):
		  True    False
		------  -------
		 0.189    0.978
	!f1 (micro=0.225, macro=0.583):
		  True    False
		------  -------
		 0.978    0.189
	accuracy (micro=0.957, macro=0.957):
		  True    False
		------  -------
		 0.957    0.957
	fpr (micro=0.852, macro=0.447):
		  True    False
		------  -------
		 0.001    0.893
	roc_auc (micro=0.895, macro=0.894):
		  True    False
		------  -------
		 0.892    0.895
	pr_auc (micro=0.968, macro=0.728):
		  True    False
		------  -------
		 0.464    0.992
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

