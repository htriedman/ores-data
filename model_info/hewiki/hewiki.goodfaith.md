Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 300, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=17634):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17337  -->    17307        30
		False      297  -->      242        55
	rates:
		              True    False
		----------  ------  -------
		sample       0.983    0.017
		population   0.972    0.028
	match_rate (micro=0.965, macro=0.5):
		  True    False
		------  -------
		 0.993    0.007
	filter_rate (micro=0.035, macro=0.5):
		  True    False
		------  -------
		 0.007    0.993
	recall (micro=0.975, macro=0.592):
		  True    False
		------  -------
		 0.998    0.185
	!recall (micro=0.208, macro=0.592):
		  True    False
		------  -------
		 0.185    0.998
	precision (micro=0.971, macro=0.867):
		  True    False
		------  -------
		 0.977    0.756
	!precision (micro=0.762, macro=0.867):
		  True    False
		------  -------
		 0.756    0.977
	f1 (micro=0.968, macro=0.642):
		  True    False
		------  -------
		 0.987    0.298
	!f1 (micro=0.317, macro=0.642):
		  True    False
		------  -------
		 0.298    0.987
	accuracy (micro=0.975, macro=0.975):
		  True    False
		------  -------
		 0.975    0.975
	fpr (micro=0.792, macro=0.408):
		  True    False
		------  -------
		 0.815    0.002
	roc_auc (micro=0.958, macro=0.925):
		  True    False
		------  -------
		  0.96    0.889
	pr_auc (micro=0.979, macro=0.726):
		  True    False
		------  -------
		 0.994    0.458
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

