Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19255):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       379  -->      139       240
		False    18876  -->      332     18544
	rates:
		              True    False
		----------  ------  -------
		sample       0.02     0.98
		population   0.022    0.978
	match_rate (micro=0.954, macro=0.5):
		  True    False
		------  -------
		 0.025    0.975
	filter_rate (micro=0.046, macro=0.5):
		  True    False
		------  -------
		 0.975    0.025
	recall (micro=0.969, macro=0.675):
		  True    False
		------  -------
		 0.367    0.982
	!recall (micro=0.38, macro=0.675):
		  True    False
		------  -------
		 0.982    0.367
	precision (micro=0.972, macro=0.65):
		  True    False
		------  -------
		 0.315    0.986
	!precision (micro=0.329, macro=0.65):
		  True    False
		------  -------
		 0.986    0.315
	f1 (micro=0.97, macro=0.661):
		  True    False
		------  -------
		 0.339    0.984
	!f1 (micro=0.353, macro=0.661):
		  True    False
		------  -------
		 0.984    0.339
	accuracy (micro=0.969, macro=0.969):
		  True    False
		------  -------
		 0.969    0.969
	fpr (micro=0.62, macro=0.325):
		  True    False
		------  -------
		 0.018    0.633
	roc_auc (micro=0.919, macro=0.919):
		  True    False
		------  -------
		 0.919    0.919
	pr_auc (micro=0.982, macro=0.625):
		  True    False
		------  -------
		 0.252    0.998
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

