Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 100, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18150):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       557  -->      396       161
		False    17593  -->     1122     16471
	rates:
		              True    False
		----------  ------  -------
		sample       0.031    0.969
		population   0.03     0.97
	match_rate (micro=0.892, macro=0.5):
		  True    False
		------  -------
		 0.083    0.917
	filter_rate (micro=0.108, macro=0.5):
		  True    False
		------  -------
		 0.917    0.083
	recall (micro=0.929, macro=0.824):
		  True    False
		------  -------
		 0.711    0.936
	!recall (micro=0.718, macro=0.824):
		  True    False
		------  -------
		 0.936    0.711
	precision (micro=0.969, macro=0.623):
		  True    False
		------  -------
		 0.256    0.991
	!precision (micro=0.278, macro=0.623):
		  True    False
		------  -------
		 0.991    0.256
	f1 (micro=0.945, macro=0.67):
		  True    False
		------  -------
		 0.377    0.963
	!f1 (micro=0.394, macro=0.67):
		  True    False
		------  -------
		 0.963    0.377
	accuracy (micro=0.929, macro=0.929):
		  True    False
		------  -------
		 0.929    0.929
	fpr (micro=0.282, macro=0.176):
		  True    False
		------  -------
		 0.064    0.289
	roc_auc (micro=0.932, macro=0.931):
		  True    False
		------  -------
		  0.93    0.932
	pr_auc (micro=0.983, macro=0.755):
		  True    False
		------  -------
		 0.513    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

