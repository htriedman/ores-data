Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.1, 'loss': 'deviance', 'max_depth': 3, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18731):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True      2130  -->     1914       216
		False    16601  -->     1849     14752
	rates:
		              True    False
		----------  ------  -------
		sample       0.114    0.886
		population   0.113    0.887
	match_rate (micro=0.732, macro=0.5):
		  True    False
		------  -------
		   0.2      0.8
	filter_rate (micro=0.268, macro=0.5):
		  True    False
		------  -------
		   0.8      0.2
	recall (micro=0.89, macro=0.894):
		  True    False
		------  -------
		 0.899    0.889
	!recall (micro=0.897, macro=0.894):
		  True    False
		------  -------
		 0.889    0.899
	precision (micro=0.932, macro=0.746):
		  True    False
		------  -------
		 0.506    0.986
	!precision (micro=0.56, macro=0.746):
		  True    False
		------  -------
		 0.986    0.506
	f1 (micro=0.902, macro=0.791):
		  True    False
		------  -------
		 0.647    0.935
	!f1 (micro=0.68, macro=0.791):
		  True    False
		------  -------
		 0.935    0.647
	accuracy (micro=0.89, macro=0.89):
		  True    False
		------  -------
		  0.89     0.89
	fpr (micro=0.103, macro=0.106):
		  True    False
		------  -------
		 0.111    0.101
	roc_auc (micro=0.96, macro=0.96):
		  True    False
		------  -------
		 0.959    0.961
	pr_auc (micro=0.973, macro=0.9):
		  True    False
		------  -------
		 0.806    0.995
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

