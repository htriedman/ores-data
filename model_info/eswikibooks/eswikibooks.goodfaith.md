Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.5, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=18731):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     17100  -->    16790       310
		False     1631  -->      414      1217
	rates:
		              True    False
		----------  ------  -------
		sample       0.913    0.087
		population   0.914    0.086
	match_rate (micro=0.847, macro=0.5):
		  True    False
		------  -------
		 0.919    0.081
	filter_rate (micro=0.153, macro=0.5):
		  True    False
		------  -------
		 0.081    0.919
	recall (micro=0.962, macro=0.864):
		  True    False
		------  -------
		 0.982    0.746
	!recall (micro=0.766, macro=0.864):
		  True    False
		------  -------
		 0.746    0.982
	precision (micro=0.961, macro=0.886):
		  True    False
		------  -------
		 0.976    0.795
	!precision (micro=0.811, macro=0.886):
		  True    False
		------  -------
		 0.795    0.976
	f1 (micro=0.961, macro=0.874):
		  True    False
		------  -------
		 0.979     0.77
	!f1 (micro=0.788, macro=0.874):
		  True    False
		------  -------
		  0.77    0.979
	accuracy (micro=0.962, macro=0.962):
		  True    False
		------  -------
		 0.962    0.962
	fpr (micro=0.234, macro=0.136):
		  True    False
		------  -------
		 0.254    0.018
	roc_auc (micro=0.981, macro=0.965):
		  True    False
		------  -------
		 0.984    0.945
	pr_auc (micro=0.975, macro=0.903):
		  True    False
		------  -------
		  0.99    0.815
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

