Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': ['A', 'B', 'C', 'D', 'E'], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 5, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=8970):
		label       n         ~A    ~B    ~C    ~D    ~E
		-------  ----  ---  ----  ----  ----  ----  ----
		'A'       895  -->   761    80    53     1     0
		'B'       786  -->    92   443   231    20     0
		'C'      2295  -->    37   132  1993   128     5
		'D'      1992  -->     0     8   117  1724   143
		'E'      3002  -->     0     0     4    82  2916
	rates:
		          'A'    'B'    'C'    'D'    'E'
		------  -----  -----  -----  -----  -----
		sample    0.1  0.088  0.256  0.222  0.335
	match_rate (micro=0.247, macro=0.2):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.099  0.074  0.267  0.218  0.342
	filter_rate (micro=0.753, macro=0.8):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.901  0.926  0.733  0.782  0.658
	recall (micro=0.874, macro=0.824):
		   A      B      C      D      E
		----  -----  -----  -----  -----
		0.85  0.564  0.868  0.865  0.971
	!recall (micro=0.965, macro=0.968):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.984  0.973  0.939  0.967  0.975
	precision (micro=0.871, macro=0.838):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.855  0.668  0.831  0.882  0.952
	!precision (micro=0.97, macro=0.969):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.983  0.959  0.954  0.962  0.985
	f1 (micro=0.872, macro=0.83):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.853  0.611  0.849  0.874  0.961
	!f1 (micro=0.967, macro=0.968):
		    A      B      C      D     E
		-----  -----  -----  -----  ----
		0.984  0.966  0.947  0.964  0.98
	accuracy (micro=0.95, macro=0.949):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.971  0.937  0.921  0.944  0.974
	fpr (micro=0.035, macro=0.032):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.016  0.027  0.061  0.033  0.025
	roc_auc (micro=0.976, macro=0.972):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.981  0.951  0.967  0.972  0.989
	pr_auc (micro=0.919, macro=0.884):
		    A      B      C      D      E
		-----  -----  -----  -----  -----
		0.901  0.706  0.911  0.916  0.987
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'A': {'type': 'number'}, 'B': {'type': 'number'}, 'C': {'type': 'number'}, 'D': {'type': 'number'}, 'E': {'type': 'number'}}}}}

