Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19230):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       745  -->      420       325
		False    18485  -->      657     17828
	rates:
		              True    False
		----------  ------  -------
		sample       0.039    0.961
		population   0.034    0.966
	match_rate (micro=0.916, macro=0.5):
		  True    False
		------  -------
		 0.054    0.946
	filter_rate (micro=0.084, macro=0.5):
		  True    False
		------  -------
		 0.946    0.054
	recall (micro=0.951, macro=0.764):
		  True    False
		------  -------
		 0.564    0.964
	!recall (micro=0.577, macro=0.764):
		  True    False
		------  -------
		 0.964    0.564
	precision (micro=0.963, macro=0.672):
		  True    False
		------  -------
		 0.359    0.984
	!precision (micro=0.381, macro=0.672):
		  True    False
		------  -------
		 0.984    0.359
	f1 (micro=0.956, macro=0.707):
		  True    False
		------  -------
		 0.439    0.974
	!f1 (micro=0.457, macro=0.707):
		  True    False
		------  -------
		 0.974    0.439
	accuracy (micro=0.951, macro=0.951):
		  True    False
		------  -------
		 0.951    0.951
	fpr (micro=0.423, macro=0.236):
		  True    False
		------  -------
		 0.036    0.436
	roc_auc (micro=0.922, macro=0.922):
		  True    False
		------  -------
		 0.922    0.922
	pr_auc (micro=0.978, macro=0.723):
		  True    False
		------  -------
		 0.449    0.997
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

