Model Information:
	 - type: GradientBoosting
	 - version: 0.5.1
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 700, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(False, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19230):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True     18724  -->    18404       320
		False      506  -->      261       245
	rates:
		              True    False
		----------  ------  -------
		sample       0.974    0.026
		population   0.967    0.033
	match_rate (micro=0.937, macro=0.5):
		  True    False
		------  -------
		 0.968    0.032
	filter_rate (micro=0.063, macro=0.5):
		  True    False
		------  -------
		 0.032    0.968
	recall (micro=0.967, macro=0.734):
		  True    False
		------  -------
		 0.983    0.484
	!recall (micro=0.501, macro=0.734):
		  True    False
		------  -------
		 0.484    0.983
	precision (micro=0.966, macro=0.736):
		  True    False
		------  -------
		 0.982     0.49
	!precision (micro=0.506, macro=0.736):
		  True    False
		------  -------
		  0.49    0.982
	f1 (micro=0.966, macro=0.735):
		  True    False
		------  -------
		 0.983    0.487
	!f1 (micro=0.503, macro=0.735):
		  True    False
		------  -------
		 0.487    0.983
	accuracy (micro=0.967, macro=0.967):
		  True    False
		------  -------
		 0.967    0.967
	fpr (micro=0.499, macro=0.266):
		  True    False
		------  -------
		 0.516    0.017
	roc_auc (micro=0.924, macro=0.924):
		  True    False
		------  -------
		 0.924    0.924
	pr_auc (micro=0.979, macro=0.735):
		  True    False
		------  -------
		 0.997    0.474
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

