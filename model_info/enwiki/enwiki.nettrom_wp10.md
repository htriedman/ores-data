Model Information:
	 - type: GradientBoosting
	 - version: 0.9.2
	 - params: {'scale': True, 'center': True, 'labels': ['Stub', 'Start', 'C', 'B', 'GA', 'FA'], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': None}
	Environment:
	 - revscoring_version: '2.11.0'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=32284):
		label       n         ~Stub    ~Start    ~C    ~B    ~GA    ~FA
		-------  ----  ---  -------  --------  ----  ----  -----  -----
		'Stub'   5415  -->     4589       791    23    11      1      0
		'Start'  5440  -->      670      3504   852   349     63      2
		'C'      5466  -->       65       943  2703  1063    603     89
		'B'      5472  -->       35       651  1362  2176    892    356
		'GA'     5495  -->        3        38   306   330   3527   1291
		'FA'     4996  -->        1         2    22   235    900   3836
	rates:
		              'Stub'    'Start'    'C'    'B'    'GA'    'FA'
		----------  --------  ---------  -----  -----  ------  ------
		sample         0.168      0.169  0.169  0.169    0.17   0.155
		population     0.576      0.322  0.054  0.035    0.01   0.003
	match_rate (micro=0.386, macro=0.189):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.501    0.269  0.117  0.085  0.097  0.066
	filter_rate (micro=0.614, macro=0.811):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.499    0.731  0.883  0.915  0.903  0.934
	recall (micro=0.745, macro=0.632):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.847    0.644  0.495  0.398  0.642  0.768
	!recall (micro=0.945, macro=0.926):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.971     0.91  0.904  0.926  0.908  0.936
	precision (micro=0.83, macro=0.372):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.976    0.772  0.229  0.161  0.065  0.031
	!precision (micro=0.845, macro=0.935):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.824    0.843  0.969  0.977  0.996  0.999
	f1 (micro=0.775, macro=0.388):
		  Stub    Start      C      B     GA    FA
		------  -------  -----  -----  -----  ----
		 0.907    0.702  0.313  0.229  0.118  0.06
	!f1 (micro=0.891, macro=0.928):
		  Stub    Start      C      B    GA     FA
		------  -------  -----  -----  ----  -----
		 0.892    0.875  0.935  0.951  0.95  0.967
	accuracy (micro=0.875, macro=0.893):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		   0.9    0.824  0.882  0.908  0.906  0.936
	fpr (micro=0.055, macro=0.074):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.029     0.09  0.096  0.074  0.092  0.064
	roc_auc (micro=0.942, macro=0.906):
		  Stub    Start      C      B    GA     FA
		------  -------  -----  -----  ----  -----
		 0.978    0.905  0.857  0.831  0.91  0.954
	pr_auc (micro=0.842, macro=0.401):
		  Stub    Start      C      B     GA     FA
		------  -------  -----  -----  -----  -----
		 0.983    0.788  0.251  0.175  0.128  0.078
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'string'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'Stub': {'type': 'number'}, 'Start': {'type': 'number'}, 'C': {'type': 'number'}, 'B': {'type': 'number'}, 'GA': {'type': 'number'}, 'FA': {'type': 'number'}}}}}

