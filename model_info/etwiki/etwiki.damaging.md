Model Information:
	 - type: GradientBoosting
	 - version: 0.5.0
	 - params: {'scale': True, 'center': True, 'labels': [True, False], 'multilabel': False, 'population_rates': None, 'ccp_alpha': 0.0, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.01, 'loss': 'deviance', 'max_depth': 7, 'max_features': 'log2', 'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 500, 'n_iter_no_change': None, 'presort': 'deprecated', 'random_state': None, 'subsample': 1.0, 'tol': 0.0001, 'validation_fraction': 0.1, 'verbose': 0, 'warm_start': False, 'label_weights': OrderedDict([(True, 10)])}
	Environment:
	 - revscoring_version: '2.8.2'
	 - platform: 'Linux-5.10.0-0.bpo.3-amd64-x86_64-with-debian-10.10'
	 - machine: 'x86_64'
	 - version: '#1 SMP Debian 5.10.13-1~bpo10+1 (2021-02-11)'
	 - system: 'Linux'
	 - processor: ''
	 - python_build: ('default', 'Jan 22 2021 20:04:44')
	 - python_compiler: 'GCC 8.3.0'
	 - python_branch: ''
	 - python_implementation: 'CPython'
	 - python_revision: ''
	 - python_version: '3.7.3'
	 - release: '5.10.0-0.bpo.3-amd64'
	
	Statistics:
	counts (n=19117):
		label        n         ~True    ~False
		-------  -----  ---  -------  --------
		True       514  -->      301       213
		False    18603  -->      399     18204
	rates:
		              True    False
		----------  ------  -------
		sample       0.027    0.973
		population   0.026    0.974
	match_rate (micro=0.94, macro=0.5):
		  True    False
		------  -------
		 0.036    0.964
	filter_rate (micro=0.06, macro=0.5):
		  True    False
		------  -------
		 0.964    0.036
	recall (micro=0.968, macro=0.782):
		  True    False
		------  -------
		 0.586    0.979
	!recall (micro=0.596, macro=0.782):
		  True    False
		------  -------
		 0.979    0.586
	precision (micro=0.974, macro=0.706):
		  True    False
		------  -------
		 0.423    0.989
	!precision (micro=0.438, macro=0.706):
		  True    False
		------  -------
		 0.989    0.423
	f1 (micro=0.971, macro=0.737):
		  True    False
		------  -------
		 0.491    0.984
	!f1 (micro=0.504, macro=0.737):
		  True    False
		------  -------
		 0.984    0.491
	accuracy (micro=0.968, macro=0.968):
		  True    False
		------  -------
		 0.968    0.968
	fpr (micro=0.404, macro=0.218):
		  True    False
		------  -------
		 0.021    0.414
	roc_auc (micro=0.963, macro=0.962):
		  True    False
		------  -------
		 0.961    0.963
	pr_auc (micro=0.988, macro=0.792):
		  True    False
		------  -------
		 0.585    0.999
	
	 - score_schema: {'title': 'Scikit learn-based classifier score with probability', 'type': 'object', 'properties': {'prediction': {'description': 'The most likely label predicted by the estimator', 'type': 'boolean'}, 'probability': {'description': 'A mapping of probabilities onto each of the potential output labels', 'type': 'object', 'properties': {'true': {'type': 'number'}, 'false': {'type': 'number'}}}}}

